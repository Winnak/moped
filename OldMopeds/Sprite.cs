﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace OldMopeds
{
    /// <summary>
    /// Contains the rendering instruction of a <see cref="GameObject"/>.
    /// </summary>
    public class Sprite : IDisposable
    {
        private Texture2D texture;
        private Rectangle srcRectangle;
        private Color color;
        private Vector2 origin;
        private Vector2 scale;
        private int zIndex;
        private SpriteEffects effect;
        private Animation animation;
        private Dictionary<string, Animation> animations = new Dictionary<string, Animation>();

        /// <summary>
        /// Gets or sets the <see cref="Texture2D"/>.
        /// </summary>
        public Texture2D Texture
        {
            get { return this.texture; }
            set { this.texture = value; }
        }

        /// <summary>
        /// Gets or sets the source <see cref="Rectangle"/>.
        /// </summary>
        public Rectangle SrcRectangle
        {
            get { return this.srcRectangle; }
            set { this.srcRectangle = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="Color"/>.
        /// </summary>
        public Color Color
        {
            get { return this.color; }
            set { this.color = value; }
        }

        /// <summary>
        /// Gets or sets the origin of the <see cref="Sprite" /> image.
        /// </summary>
        public Vector2 Origin
        {
            get { return this.origin; }
            set { this.origin = value; }
        }

        /// <summary>
        /// Gets or sets the scale.
        /// </summary>
        public Vector2 Scale
        {
            get { return this.scale; }
            set { this.scale = value; }
        }

        /// <summary>
        /// Gets or sets the Z index.
        /// </summary>
        public int ZIndex
        {
            get { return this.zIndex; }
            set { this.zIndex = value; }
        }

        /// <summary>
        /// Gets or sets the sprite effects.
        /// </summary>
        public SpriteEffects Effect
        {
            get { return this.effect; }
            set { this.effect = value; }
        }

        /// <summary>
        /// Gets or sets the animation.
        /// </summary>
        public Animation Animation
        {
            get { return this.animation; }
            set { this.animation = value; }
        }

        /// <summary>
        /// Gets or sets the animation dictionary.
        /// </summary>
        public Dictionary<string, Animation> Animations
        {
            get { return this.animations; }
            set { this.animations = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Sprite"/> class.
        /// </summary>
        /// <param name="texture">Sprite texture.</param>
        public Sprite(Texture2D texture)
        {
            this.color = Color.White;
            this.origin = Vector2.Zero;
            this.scale = Vector2.One;
            this.zIndex = 0;
            this.effect = SpriteEffects.None;

            this.texture = texture;
            this.srcRectangle = new Rectangle(0, 0, texture.Width, texture.Height);
        }
        
        /// <summary>
        /// Finalizes an instance of the <see cref="Sprite" /> class.
        /// </summary>
        ~Sprite()
        {
            this.Dispose(false);
        }

        /// <summary>
        /// Moves the <see cref="srcRectangle"/> to the next frame.
        /// </summary>
        /// <param name="gameTime">Game time.</param>
        public void Animate(GameTime gameTime)
        {
            if (this.animation != null)
            {
                this.srcRectangle = this.animation.Animate(gameTime);
            }
        }

        /// <summary>
        /// Adds animation set to the <see cref="animations" /> dictionary.
        /// </summary>
        /// <param name="animName">Animation key name.</param>
        /// <param name="firstFrame">First frame.</param>
        /// <param name="animFrames">Number of frames.</param>
        /// <param name="animSpeed">Speed of animation.</param>
        /// <param name="origin">Origin of the <see cref="Spirte"/>.</param>
        /// <param name="loops">Looping animation.</param>
        public void AddAnimation(string animName, Rectangle firstFrame, int animFrames, float animSpeed, Vector2 origin, bool loops = true)
        {
            this.animations.Add(animName,
                new Animation(firstFrame, animFrames, animSpeed, origin, loops));

            if (this.animation == null)
            {
                this.PlayAnimation(this.animations.Keys.First());
            }
        }

        /// <summary>
        /// Plays an <see cref="Animation"/>.
        /// </summary>
        /// <param name="animName">Animation key name.</param>
        public void PlayAnimation(string animName)
        {
            if (this.animations.ContainsKey(animName))
            {
                this.animation = this.animations[animName];
                this.origin = this.animation.origin;
            }
        }

        /// <summary>
        /// Rewinds the animation to the start.
        /// </summary>
        /// <param name="animName">Animation key name.</param>
        public void RewindAnimation(string animName)
        {
            if (this.animations.ContainsKey(animName))
            {
                this.animations[animName].timeElapsed = 0;
            }
        }

        /// <summary>
        /// Disposes the <see cref="Sprite"/>.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes the <see cref="Sprite"/>.
        /// </summary>
        /// <param name="forced">If disposing is called.</param>
        protected virtual void Dispose(bool forced) // CA1063.
        {

        }
    }
}
