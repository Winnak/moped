﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace OldMopeds
{
    /// <summary>
    /// Translates the world space and converts between those two.
    /// </summary>
    public class Camera : IDisposable
    {
        private const float MinZoom = 1.00f;
        private const float MaxZoom = 0.35f;

        private Size vRes;
        private Size rRes;

        private Vector2 position  = Vector2.Zero;
        private float zoom        = 1f;
        private float rotation    = 0f;
        private Matrix  transform = Matrix.Identity;

        private Matrix camTranslation = Matrix.Identity;
        private Matrix camRotation    = Matrix.Identity;
        private Matrix camScale       = Matrix.Identity;
        private Matrix resTranslation = Matrix.Identity;

        private Vector3 camTranslationPosition;
        private Vector3 camTranslationScale;
        private VirtualResolution render;

        private bool needUpdate;

        public Size VirtualResolution
        {
            get { return vRes; }
            set { vRes = value; }
        }
        public Vector2 Position
        {
            get { return position; }
            set { this.needUpdate = true; position = value; }
        }
        public float Zoom
        {
            get { return zoom; }
            set { zoom = MathHelper.Clamp(value, MaxZoom, MinZoom); }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Camera"/> class.
        /// </summary>
        /// <param name="render">Virtual renderer</param>
        public Camera(VirtualResolution render)
        {
            this.rRes = render.VirtualSize;
            this.vRes = render.VirtualSize;
            this.render = render;
            this.needUpdate = true;
        }

        ~Camera()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the transformation matrix.
        /// </summary>
        /// <returns>Tranformation Matrix</returns>
        public Matrix GetViewTransformation()
        {
            if (needUpdate)
            {
                camTranslationPosition.X = -this.position.X;
                camTranslationPosition.Y = -this.position.Y;

                Matrix.CreateTranslation(ref camTranslationPosition, out camTranslation);
                Matrix.CreateRotationZ(this.rotation, out this.camRotation);

                this.camTranslationScale.X = this.zoom;
                this.camTranslationScale.Y = this.zoom;
                this.camTranslationScale.Z = 1;

                Matrix.CreateScale(ref camTranslationScale, out camScale);

                camTranslationPosition.X = render.VirtualSize.Width  * 0.5f;
                camTranslationPosition.Y = render.VirtualSize.Height * 0.5f;
                camTranslationPosition.Z = 0;

                Matrix.CreateTranslation(ref camTranslationPosition, out resTranslation);

                this.transform = this.camTranslation *
                                 this.camRotation    *
                                 this.camScale       *
                                 this.resTranslation *
                                 this.render.GetTransformation();

                needUpdate = false;
            }

            return this.transform;
        }

        /// <summary>
        /// Updates the <see cref="Camera" />
        /// </summary>
        /// <param name="time">Current game time.</param>
        public void Update(GameTime time)
        {
            render.GetTransformation();
        }

        /// <summary>
        /// Converts virtual coordinates to screen coordinates
        /// </summary>
        /// <param name="virtualPosition">Screen coordinates</param>
        public Point ToDisplay(Vector2 virtualPosition)
        {
            Vector2 vec = Vector2.Transform(
                    new Vector2(virtualPosition.X, virtualPosition.Y) + 
                    new Vector2(render.ViewPort.X, render.ViewPort.Y), 
                render.GetTransformation());
            return new Point((int)vec.X, (int)vec.Y);
        }

        /// <summary>
        /// Converts screen coordinates to virtual coordinates
        /// </summary>
        /// <param name="virtualPosition">Screen coordinates</param>
        public Vector2 ToVirtual(Point displayPosition)
        {
            Vector2 position = new Vector2(displayPosition.X, displayPosition.Y);

            position = position - new Vector2(render.ViewPort.X, render.ViewPort.Y);

            return Vector2.Transform(position, Matrix.Invert(GetViewTransformation()));
        }

        /// <summary>
        /// Disposes the Camera.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes the Camera.
        /// </summary>
        /// <param name="forced">Did it not happen automaticly?</param>
        protected virtual void Dispose(bool forced)
        {
        }
    }
}
