﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace OldMopeds
{
    /// <summary>
    /// A game object is a super class for all classes that requires updating.
    /// </summary>
    public abstract class GameObject : IDisposable
    {
        private Sprite sprite;
        private bool drawInGUI = false;

        public Sprite Sprite
        {
            get { return sprite; }
            set { sprite = value; }
        }
        public bool DrawInGUI
        {
            get { return drawInGUI; }
            set { drawInGUI = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameObject"/> class.
        /// </summary>
        public GameObject()
        {
            GameManager.Objects.Add(this);
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="GameObject" /> class.
        /// </summary>
        ~GameObject()
        {
            this.Dispose(false);
        }

        /// <summary>
        /// Updates the <see cref="GameObject" />
        /// </summary>
        /// <param name="gametime">Current game time.</param>
        public abstract void Update(GameTime time);

        /// <summary>
        /// Draws this instance of the <see cref="GameObject" /> class.
        /// </summary>
        /// <param name="sb">SpriteBatch used to draw textures.</param>
        public abstract void Draw(GameTime time, SpriteBatch sb);

        /// <summary>
        /// Disposes the GameObject.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes the game object.
        /// </summary>
        /// <param name="forced">Did it not happen automatically?</param>
        protected virtual void Dispose(bool forced)
        {
            GameManager.Objects.Remove(this);

            if (this.sprite != null)
                this.sprite.Dispose();
        }
    }
}
