﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace OldMopeds
{
    /// <summary>
    /// Preloads all assets.
    /// </summary>
    public static class Asset
    {
		#if WINDOWS
		private const string ContentPath = "Content\\";
		#endif
		#if MACINTOSH
		private const string ContentPath = "";
		#else
		private const string ContentPath = "Content/";
		#endif

        private static ContentManager content;
        private static Dictionary<string, SpriteFont> font = new Dictionary<string, SpriteFont>();
        private static Dictionary<string, Texture2D> image = new Dictionary<string, Texture2D>();
        private static Dictionary<string, SoundEffect> sound = new Dictionary<string, SoundEffect>();

        /// <summary>
        /// Gets or sets the content manager.
        /// </summary>
        public static ContentManager Content
        {
            get { return Asset.content; }
            set { Asset.content = value; }
        }
        /// <summary>
        /// Gets a preloaded font.
        /// </summary>
        public static Dictionary<string, SpriteFont> Font
        {
            get { return Asset.font; }
        }
        /// <summary>
        /// Gets a preloaded image.
        /// </summary>
        public static Dictionary<string, Texture2D> Image
        {
            get { return Asset.image; }
        }
        /// <summary>
        /// Gets a preloaded sound.
        /// </summary>
        public static Dictionary<string, SoundEffect> Sound
        {
            get { return Asset.sound; }
        }

        /// <summary>
        /// Preloads assets.
        /// </summary>
        public static void LoadAssets()
        {
#if DEBUG
			var asdg = Directory.GetCurrentDirectory ();
			var som = Directory.GetDirectories(asdg);
#endif
            // Fonts
			if (Directory.Exists(ContentPath + @"Fonts"))
            {
				var fontFiles = Directory.GetFiles(ContentPath + @"Fonts");

                foreach (var item in fontFiles)
                {
                    if (!item.Contains(".xnb"))
                        continue; //possible flaw: if file is called ".xnb.somethingelse"

					var name = StripPath (item);

					var path = item.Substring(ContentPath.Length);
                    path = path.Substring(0, path.LastIndexOf('.'));
                    
					font.Add(name, content.Load<SpriteFont>(path));
                }
            }

			if (Directory.Exists(ContentPath + @"Images"))
            {
                var imageFiles = Directory.GetFiles(ContentPath + @"Images");

                // Images
                foreach (var item in imageFiles)
                {
                    if (!item.Contains(".png"))
                        continue; //possible flaw: if file is called ".png.somethingelse"

					var name = StripPath (item);

					var path = item.Substring(ContentPath.Length);
					path = path.Substring(0, path.LastIndexOf('.'));

					image.Add(name, content.Load<Texture2D>(path));
                }
            }

            if (Directory.Exists(ContentPath + @"Sounds"))
            {
                var soundFiles = Directory.GetFiles(ContentPath + @"Sounds");
                
                // Sounds
                foreach (var item in soundFiles)
                {
                    if (!item.Contains(".wav"))
                        continue; //possible flaw: if file is called ".wav.somethingelse"

					var name = StripPath (item);

					var path = item.Substring(ContentPath.Length);
					path = path.Substring(0, path.LastIndexOf('.'));

					sound.Add(name, content.Load<SoundEffect>(path));
                }                
            }
        }

		/// <summary>
		/// Strips the file path.
		/// </summary>
		/// <returns>The file name.</returns>
		/// <param name="path">The path.</param>
		private static string StripPath (string path) 
		{
			#if WINDOWS
			var name = path.Substring(path.LastIndexOf('\\') + 1);
			#else
			var name = path.Substring(path.LastIndexOf('/') + 1);
			#endif
            name = name.Substring(0, name.LastIndexOf('.'));

			return name;
		}
    }
}
