﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace OldMopeds
{
    /// <summary>
    /// Seperates world space from screen space.
    /// </summary>
    public class VirtualResolution : IDisposable
    {
        private GraphicsDeviceManager graphics;
        private Size res;
        private Size vres;
        private Matrix scaleMatrix;
        private bool modified;
        private Viewport viewport;

        public Viewport ViewPort
        {
            get { return viewport; }
            set { viewport = value; }
        }
        public Size VirtualSize
        {
            get { return this.vres; }
            set { this.vres = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VirtualResolution"/> class.
        /// </summary>
        /// <param name="device">Graphics Device Manager.</param>
        /// <param name="virtualSpace">Size of the virtual space.</param>
        public VirtualResolution(GraphicsDeviceManager device, Size virtualSpace)
        {
            this.scaleMatrix = Matrix.Identity;

            this.graphics = device;
            this.res.Width = graphics.PreferredBackBufferWidth;
            this.res.Height = graphics.PreferredBackBufferHeight;
            this.vres.Width = virtualSpace.Width;
            this.vres.Height = virtualSpace.Height;
            this.modified = true;
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="VirtualResolution" /> class.
        /// </summary>
        ~VirtualResolution()
        {
            this.Dispose(false);
        }

        /// <summary>
        /// Gets the transformation matrix.
        /// </summary>
        /// <returns>Returns transformation matrix.</returns>
        public Matrix GetTransformation()
        {
            if (this.modified) // To avoid recreating the matrix when it is uneeded.
                Matrix.CreateScale(res.Width / vres.Width, res.Height / vres.Height, 
                                   1f, out this.scaleMatrix);

            return this.scaleMatrix;
        }

        /// <summary>
        /// Setup viewport to real screen size
        /// </summary>
        public void SetupFullViewport()
        {
            var vp      = new Viewport();
            vp.X = vp.Y = 0;
            vp.Width    = res.Width;
            vp.Height   = res.Height;
            graphics.GraphicsDevice.Viewport = vp;
            modified    = true;
        }

        /// <summary>
        /// Sets up the virtual space.
        /// </summary>
        public void SetupVirtualScreenViewport()
        {
            var targetAspectRatio = vres.Width / (float)vres.Height;
            // figure out the largest area that fits in this resolution at the desired aspect ratio
            var width  = res.Width;
            var height = (int)(width / targetAspectRatio + .5f);

            if (height > res.Height)
            {
                height = res.Height;
                 width = (int)(height * targetAspectRatio + .5f);
            }

            // set up the new viewport centered in the backbuffer
            viewport = new Viewport
            {
                X = (res.Width / 2)  - (width / 2),
                Y = (res.Height / 2) - (height / 2),
                Width = width,
                Height = height
            };

           graphics.GraphicsDevice.Viewport = viewport;
        }

        /// <summary>
        /// Draws the two world spaces.
        /// </summary>
        public void Draw()
        {
            SetupFullViewport();
            graphics.GraphicsDevice.Clear(Color.CornflowerBlue);
            SetupVirtualScreenViewport();
        }

        /// <summary>
        /// Disposes the VirtualResolution.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes the VirtualResolution.
        /// </summary>
        /// <param name="forced">Did it not happen automatically?</param>
        private void Dispose(bool forced)
        {
        }
    }
}
