﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace OldMopeds
{
    /// <summary>
    /// The main type for our game.
    /// </summary>
    public class MopedGame : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        /// <summary>
        /// Initializes a new instance of the <see cref="MopedGame" /> class.
		/// </summary>
        public MopedGame()
        {
            graphics = new GraphicsDeviceManager(this);
			#if WINDOWS || LINUX
			Content.RootDirectory = "Content";
			#endif
			#if MACINTOSH
			Content.RootDirectory = "MacMoped.app/Contents/Resources";
			#endif

        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            //iPhone 5 screen size.
            GameManager.GameSize = new Size(1136, 640);
            graphics.PreferredBackBufferWidth = GameManager.GameSize.Width;
            graphics.PreferredBackBufferHeight = GameManager.GameSize.Height;
            this.IsMouseVisible = true;
			this.Window.AllowUserResizing = true;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Asset.Content = this.Content;
            Asset.LoadAssets();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent() { }

        /// <summary>
        /// Runs after initilize and load content, but before update and draw.
        /// </summary>
        protected override void BeginRun()
        {
            //Initilize camera
            GameManager.Renderer = new VirtualResolution(graphics, new Size(1136, 640));
            GameManager.Cam = new Camera(GameManager.Renderer);

            GameManager.Initialize();

            base.EndRun();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            GameManager.Cam.Update(gameTime);

            Input.Update(gameTime);
            GameManager.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GameManager.Renderer.Draw();

            //Backgrounds
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.NonPremultiplied, 
                SamplerState.AnisotropicClamp, DepthStencilState.Default, RasterizerState.CullNone);

            foreach (var obj in GameManager.Backgrounds)
            {
                obj.Draw(gameTime, spriteBatch);
            }

            spriteBatch.End();

            //GAME
            spriteBatch.Begin(
                SpriteSortMode.FrontToBack, BlendState.NonPremultiplied, 
                SamplerState.AnisotropicClamp, DepthStencilState.Default, 
                RasterizerState.CullNone, null, 
                GameManager.Cam.GetViewTransformation());

            foreach (var obj in GameManager.Objects)
            {
                if (obj.DrawInGUI)
                    continue;

                obj.Draw(gameTime, spriteBatch);
            }

            spriteBatch.End();

            //GUI
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.NonPremultiplied, 
                SamplerState.AnisotropicClamp, DepthStencilState.Default, RasterizerState.CullNone);

            foreach (var obj in GameManager.Objects)
            {
                if (!obj.DrawInGUI)
                    continue;

                obj.Draw(gameTime, spriteBatch);
            }

            spriteBatch.End();

#if DEBUG //Debug top bar.
            spriteBatch.Begin();
            spriteBatch.Draw(Asset.Image["pixel"], new Rectangle(0, 0, GameManager.GameSize.Width, 20), new Color(Color.Black, 0.6f));

            var text = Input.GetSwipe().ToString() +
                "  " + Input.CurrentTouchState.ToString() +
                "  " + GameManager.Cam.ToVirtual(Input.MousePosition).ToString() +
                ", " + "Camera:" +
                "  " + GameManager.Cam.Position.ToString() +
                "  " + GameManager.Cam.Zoom +
                ", " + "Time: " + Convert.ToInt32(gameTime.TotalGameTime.TotalSeconds) +
                ", " + "FPS: " + (int)(1 / gameTime.ElapsedGameTime.TotalSeconds) + 
                ", " + "Objects: " + GameManager.Objects.Count;

            spriteBatch.DrawString(Asset.Font["UbuntuMono10"], text, new Vector2(10, 0), Color.White);
            spriteBatch.End();
#endif

            base.Draw(gameTime);
        }

        /// <summary>
        /// Unused: Runs when game is shutting down.
        /// </summary>
        /// <param name="sender">Event.</param>
        /// <param name="args">Arguments.</param>
        protected override void OnExiting(object sender, EventArgs args)
        {
            base.OnExiting(sender, args);
        }
    }
}
