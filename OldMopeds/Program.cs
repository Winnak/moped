﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
#endregion
#if MACINTOSH
using System.Drawing;
using MonoMac.Foundation;
using MonoMac.AppKit;
using MonoMac.ObjCRuntime;
#endif

namespace OldMopeds
{
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
	{
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
#if WINDOWS || LINUX
            using (var game = new MopedGame())
            {
                game.Run();
            }
#endif
		}

		#if MACINTOSH
		static void Main (string[] args)
		{
			NSApplication.Init ();
			NSApplication.Main (args);
		}
		#endif
	}
}
