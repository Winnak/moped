﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace OldMopeds
{
    /// <summary>
    /// Contains the player with all his <see cref="Accessory"/>s.
    /// </summary>
    public class Player : GameObject
    {
        private int money;
        private List<Accessory> accessories;
        private List<Accessory> availableAccesories;
        private List<Component> availableComps;
        private List<Moped> mopeds;
        private Moped activeMoped;

        public Moped ActiveMoped
        {
            get { return activeMoped; }
            set { activeMoped = value; }
        }

        public int Money
        {
            get { return money; }
            set { money = value; }
        }
        public List<Accessory> Accessories
        {
            get { return accessories; }
            set { accessories = value; }
        }
        public List<Component> AvailableComps
        {
            get { return availableComps; }
            set { availableComps = value; }
        }
        public List<Accessory> AvailableAccesories
        {
            get { return availableAccesories; }
            set { availableAccesories = value; }
        }
        public List<Moped> Mopeds
        {
            get { return mopeds; }
            set { mopeds = value; }
        }

        private static Player instance;

        public static Player Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Player();
                }
                return instance;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player" /> class.
        /// </summary>
        public Player()
        {
            mopeds = new List<Moped>();
            accessories = new List<Accessory>();
            availableComps = new List<Component>();
            availableAccesories = new List<Accessory>();
        }

        /// <summary>
        /// Unused: Updates the <see cref="Player"/>.
        /// </summary>
        /// <param name="time">Current game time.</param>
        public override void Update(GameTime time)
        {
        }

        /// <summary>
        /// Unused: Draws the <see cref="Player"/>.
        /// </summary>
        /// <param name="time">Current game time.</param>
        public override void Draw(GameTime time, SpriteBatch sb)
        {
        }

        /// <summary>
        /// Unused: Change accessory of the <see cref="Player"/> instance.
        /// </summary>
        /// <param name="accessory">The new accessory.</param>
        public void ChangeAccesory(Accessory accessory)
        {
            foreach (Accessory acces in availableAccesories)
            {
                if (accessory.Type == acces.Type)
                {
                    availableAccesories.Add(acces);
                    accessories.Remove(acces);
                }
            }

            accessories.Add(accessory);
            availableAccesories.Remove(accessory);
        }
    }
}
