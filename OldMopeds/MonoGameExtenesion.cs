﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace OldMopeds
{
    /// <summary>
    /// Tries to fill the gaps in MonoGame.
    /// </summary>
    public static class MonoGameExtenesion
    {
        /* Det her er hvor c# bliver til pis.
         * Jeg ville gerne overloade + og - operatoren på rectangle, så man kan
         * bare lægge en vector til rectangle for at ændre positionen, men nej.
         * Kunne også have brugt det så man bare kunne caste en vector2 til et
         * point, men nej.
         * 
         * Jeg ville også gerne overloade constructoren på rectangle, så jeg 
         * kan bruge min Size klasse og monos vector (eller point) til at  lave
         * et Rectangle, men nej. I stedet må jeg lave shitty methoder.
         * 
         * Also man kan ikke extende med properties, wtf?
         * [Erik Høyrup Jørgensen]
         * */

        /// <summary>
        /// Translates the rectangle using a vector.
        /// </summary>
        /// <param name="rect">Extension <see cref="Rectangle"/>.</param>
        /// <param name="pos">New position of the rectangle.</param>
        /// <returns>New rectangle</returns>
        public static Rectangle ChangePosition(this Rectangle rect, Vector2 pos)
        {
            return new Rectangle(rect.X + (int)pos.X, rect.Y + (int)pos.Y, rect.Width, rect.Height);
        }

        /// <summary>
        /// Translates the rectangle using a point.
        /// </summary>
        /// <param name="rect">Extension <see cref="Rectangle"/>.</param>
        /// <param name="pos">New position of the rectangle.</param>
        /// <returns>New rectangle.</returns>
        public static Rectangle ChangePosition(this Rectangle rect, Point pos)
        {
            return new Rectangle(rect.X + (int)pos.X, rect.Y + (int)pos.Y, rect.Width, rect.Height);
        }

        /// <summary>
        /// Creates a rectangle from a point and a size.
        /// </summary>
        /// <param name="rect">Extension <see cref="Rectangle"/>.</param>
        /// <param name="position">Position of rectangle.</param>
        /// <param name="size">Size of rectangle.</param>
        /// <returns>New rectangle.</returns>
        public static Rectangle Create(this Rectangle rect, Point position, Size size)
        {
            return new Rectangle(position.X, position.Y, size.Width, size.Height);
        }

        /// <summary>
        /// Creates a rectangle from a vector and a size.
        /// </summary>
        /// <param name="rect">Extension <see cref="Rectangle"/>.</param>
        /// <param name="position">Position of rectangle.</param>
        /// <param name="size">Size of rectangle.</param>
        /// <returns>New rectangle.</returns>
        public static Rectangle Create(this Rectangle rect, Vector2 position, Size size)
        {
            return new Rectangle((int)position.X, (int)position.Y, size.Width, size.Height);
        }

        /// <summary>
        /// Gets the position of rectangle and converts it to a <see cref="Vector2"/>.
        /// </summary>
        /// <param name="rect">Extension <see cref="Rectangle"/>.</param>
        /// <returns>The position.</returns>
        public static Vector2 Position(this Rectangle rect)
        {
            return new Vector2(rect.X, rect.Y);
        }

        /// <summary>
        /// Gets the Size of a texture.
        /// </summary>
        /// <param name="text">Extension <see cref="Texture2D"/>.</param>
        /// <returns>The size of texture.</returns>
        public static Size Size(this Texture2D text)
        {
            return new Size(text.Width, text.Height);
        }

        /// <summary>
        /// Gets the size of a rectangle.
        /// </summary>
        /// <param name="rect">Extension <see cref="Rectangle"/>.</param>
        /// <returns>The size of the rectangle</returns>
        public static Size Size(this Rectangle rect)
        {
            return new Size(rect.Width, rect.Height);
        }

        /// <summary>
        /// Converts a vector to a point.
        /// </summary>
        /// <param name="vec">A Vector 2D.</param>
        /// <returns>A Point.</returns>
        public static Point ToPoint(this Vector2 vec) 
        {
            return new Point((int)vec.X, (int)vec.Y);
        }

        /// <summary>
        /// A neat way to draw a sprite.
        /// </summary>
        /// <param name="sb">Current SpriteBatch</param>
        /// <param name="s">Sprite to draw.</param>
        /// <param name="position">Vector2 position.</param>
        /// <param name="depth">Draw layer.</param>
        public static void DrawSprite(this SpriteBatch sb, Sprite s, Vector2 position, float depth)
        {
            sb.Draw(s.Texture, position, s.SrcRectangle, s.Color, 0,
                s.Origin, s.Scale, s.Effect, depth);
        }

        /// <summary>
        /// A neat way to draw a sprite.
        /// </summary>
        /// <param name="sb">Current SpriteBatch</param>
        /// <param name="s">Sprite to draw.</param>
        /// <param name="rotation">Rotation.</param>
        /// <param name="position">Vector2 position.</param>
        /// <param name="depth">Draw layer.</param>
        public static void DrawSprite(this SpriteBatch sb, Sprite s, 
                                float rotation, Vector2 position, float depth)
        {
            sb.Draw(s.Texture, position, s.SrcRectangle, s.Color, rotation,
                s.Origin, s.Scale, s.Effect, depth);
        }

		public static bool Contains (this Rectangle rect, Vector2 vec)
		{
			return (vec.X > rect.X && vec.X < rect.X + rect.Width) && 
				   (vec.Y > rect.Y && vec.Y < rect.Y + rect.Height);
		}
    }
}
