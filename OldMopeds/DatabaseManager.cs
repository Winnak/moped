﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Data;
using Microsoft.Xna.Framework;

#if WINDOWS
using System.Data.SQLite;
#else 
using Mono.Data.Sqlite;
#endif

namespace OldMopeds
{
    /// <summary>
    /// A Manager that takes care of loading in data about mopeds, 
    /// components and accessories and wether the player owns these 
    /// and how much money the player has.
    /// thereafter it contructs the mopeds, components and accessories and sends 
    /// the information to the player and market.
    /// </summary>
    public class DatabaseManager : IDisposable
    {
        private const string Path = @"MopedData";
		#if WINDOWS
        private SQLiteConnection databaseConnectionLocation = 
            new SQLiteConnection(@"Data Source=" + Path + ";Version=3;New=False;Compress=True;");
		#else
		private SqliteConnection databaseConnectionLocation = 
			new SqliteConnection(@"Data Source=" + Path + ";Version=3;New=False;Compress=True;");
		
		#endif
        private DataSet dataSet = new DataSet();

        public DataSet Data
        {
            get { return dataSet; }
            set { dataSet = value; }
        }

        /// <summary>
        /// Empty contructor as the databasemanager does not need the Initialize 
        /// anything and doesn't need to do anything on contruction.
        /// </summary>
        public DatabaseManager()
        {

        }
        ~DatabaseManager()
        {
            Dispose(false);
        }

        /// <summary>
        /// Loads MopedData.db and fills a dataset with information from it
        /// </summary>
        public void Load()
        {
            if (!File.Exists(Path))
            {
                throw new FileNotFoundException(@"[MopedData.db Couldn't be found]");
            }

            Connect(databaseConnectionLocation);

            FillContentDataSet("accessory");
            FillContentDataSet("component");
            FillContentDataSet("moped");
            FillContentDataSet("mopedcomponent");
            FillContentDataSet("spiller");
            FillContentDataSet("spilleraccessory");
            FillContentDataSet("spillercomponent");
            FillContentDataSet("spillermoped");

            Disconnect(databaseConnectionLocation);

            CreateComponents();// first of the chain: CreateComponents -> AssembleMopeds -> MakeAccessories -> DelieverToPlayer -> HandOverToMarket
        }

        /// <summary>
        /// Fills given DataSet with data from given string table name via given SQLiteConnection
        /// </summary>
        private void FillContentDataSet(string tableName)
        {
            //var table = new SQLiteParameter(tableName);

#if WINDOWS
            using (SQLiteDataAdapter da = new SQLiteDataAdapter(
                "SELECT * FROM " + tableName, databaseConnectionLocation))
#else
			using (SqliteDataAdapter da = new SqliteDataAdapter(
                "SELECT * FROM " + tableName, databaseConnectionLocation))
#endif
            {
                DataTable dt = new DataTable(tableName);
                da.Fill(dt);

                dataSet.Tables.Add(dt);
            }
        }

        /// <summary>
        /// Executes a given string of SQL commands on a given SQLiteConnection
        /// </summary>
		#if WINDOWS
        private void ExecuteQuery(SQLiteConnection con, string txtQuery)
        {
            using (SQLiteCommand sqlCmd = con.CreateCommand())
            {
                var textQuery = new SQLiteParameter(txtQuery);

                sqlCmd.CommandText = textQuery.ToString();
                sqlCmd.ExecuteNonQuery();
            }
        }
		#else
		private void ExecuteQuery(SqliteConnection con, string txtQuery)
		{
			using (SqliteCommand sqlCmd = con.CreateCommand())
			{
				sqlCmd.CommandText = txtQuery;
				sqlCmd.ExecuteNonQuery();
			}
		}
		#endif

        /// <summary>
        /// Creates the components from the database "MopedData" and inserts them into a Dictionary
        /// the Dictionary will be used to construct the mopeds, give the player her/his components and the market its items
        /// </summary>
        private void CreateComponents()
        {
            Dictionary<int, Component> components = new Dictionary<int, Component>();
            string[] stats;

            if (dataSet.Tables["component"].Rows.Count > 0)
                for (int i = 0; i < dataSet.Tables["component"].Rows.Count; i++)
                {
                    stats = new string[5];
                    int id = Convert.ToInt32(dataSet.Tables["component"].Rows[i]["Id"]);
                    string name = (string) dataSet.Tables["component"].Rows[i]["Navn"];
                    string typeName = (string) dataSet.Tables["component"].Rows[i]["Type"];
                    Component.ComponentType type = (Component.ComponentType) Enum.Parse(typeof(Component.ComponentType), typeName);
                    int price = Convert.ToInt32(dataSet.Tables["component"].Rows[i]["Price"]);

                    stats[0] = "speed=" + Convert.ToString(dataSet.Tables["component"].Rows[i]["Speed"]);
                    stats[1] = "friction=" + Convert.ToString(dataSet.Tables["component"].Rows[i]["Friction"]);
                    stats[2] = "weight=" + Convert.ToString(dataSet.Tables["component"].Rows[i]["Weight"]);
                    stats[3] = "aerodynamism=" + Convert.ToString(dataSet.Tables["component"].Rows[i]["Aerodynamism"]);
                    stats[4] = "acceleration=" + Convert.ToString(dataSet.Tables["component"].Rows[i]["Acceleration"]);

                    components.Add(id, new Component(name, type, id, price, stats));
                }

            AssembleMoped(components);
        }

        /// <summary>
        /// Assemble a moped out from the information from the Database
        /// </summary>
        /// <param name="components"></param>
        private void AssembleMoped(Dictionary<int, Component> components)
        {
            Dictionary<int, Moped> mopeds = new Dictionary<int, Moped>();

            if (dataSet.Tables["moped"].Rows.Count > 0)
                for (int i = 0; i < dataSet.Tables["moped"].Rows.Count; i++)
                {
                    int mopedId = Convert.ToInt32(dataSet.Tables["Moped"].Rows[i]["Id"]);
                    string mopedName = Convert.ToString(dataSet.Tables["Moped"].Rows[i]["Name"]);
                    int mopedPrice = Convert.ToInt32(dataSet.Tables["Moped"].Rows[i]["Price"]);


                    Dictionary<int, Component> mopedscomp = new Dictionary<int, Component>();

                    for (int n = 0; n < dataSet.Tables["mopedcomponent"].Rows.Count; n++)
                    {
                        if (Convert.ToInt32(dataSet.Tables["mopedcomponent"].Rows[n]["Id"]) == mopedId)
                        {
                            mopedscomp.Add(Convert.ToInt32(dataSet.Tables["mopedcomponent"].Rows[n]["CId"]), null);
                        }
                    }

                    foreach (int compId in mopedscomp.Keys.ToList())
                    {
                        mopedscomp[compId] = components[compId];
                    }

                    string[] mopedstats = new string[5];
                    mopedstats[0] = "speed=" + Convert.ToString(dataSet.Tables["Moped"].Rows[i]["Speed"]);
                    mopedstats[1] = "friction=" + Convert.ToString(dataSet.Tables["Moped"].Rows[i]["Friction"]);
                    mopedstats[2] = "weight=" + Convert.ToString(dataSet.Tables["Moped"].Rows[i]["Weight"]);
                    mopedstats[3] = "acceleration=" + Convert.ToString(dataSet.Tables["Moped"].Rows[i]["Acceleration"]);
                    mopedstats[4] = "aerodynamism=" + Convert.ToString(dataSet.Tables["Moped"].Rows[i]["Aerodynamism"]);

                    mopeds.Add(mopedId, new Moped(mopedName, mopedId, mopedscomp.Values.ToList(), mopedstats));
                    mopeds[mopedId].Price = mopedPrice;

                    //TODO Fjern det værste hack nogensinde
                    //GameManager.Objects.RemoveRange(26, 9);
                }

            MakeAccessories(components, mopeds);
        }

        /// <summary>
        /// makes accessories from the data provided by the database
        /// </summary>
        /// <param name="components"></param>
        /// <param name="mopeds"></param>
        private void MakeAccessories(Dictionary<int, Component> components, Dictionary<int, Moped> mopeds)
        {
            Dictionary<int, Accessory> accessories = new Dictionary<int, Accessory>();

            if (dataSet.Tables["accessory"].Rows.Count > 0)
            {
                for (int i = 0; i < dataSet.Tables["accessory"].Rows.Count; i++)
                {
                    int id = Convert.ToInt32(dataSet.Tables["accessory"].Rows[i]["Id"]);
                    string name = (string)dataSet.Tables["accessory"].Rows[i]["Navn"];
                    int price = Convert.ToInt32(dataSet.Tables["accessory"].Rows[i]["Price"]);
                    string typeName = (string)dataSet.Tables["accessory"].Rows[i]["Type"];
                    Accessory.AccessoryType type = (Accessory.AccessoryType) Enum.Parse(typeof(Accessory.AccessoryType), typeName);

                    accessories.Add(id, new Accessory(name, type, price, id));
                }
            }
            DelieverToPlayer(components, mopeds, accessories);
        }

        /// <summary>
        /// send info about the Player to the static Player class, together with <see cref="components"/> and <see cref="mopeds"/>
        /// </summary>
        /// <param name="components"></param>
        /// <param name="mopeds"></param>
        private void DelieverToPlayer(Dictionary<int, Component> components, Dictionary<int, Moped> mopeds, Dictionary<int, Accessory> accessories)
        {
            if (dataSet.Tables["spiller"].Rows.Count > 0)
            {
                List<Component> spillerComponents = new List<Component>();
                List<Component> mopedComponents = new List<Component>();
                List<int> mopedcomponentId = new List<int>();
                List<Moped> spillerMopeds = new List<Moped>();
                List<Accessory> spillerAccessories = new List<Accessory>();

                for (int i = 0; i < dataSet.Tables["spillercomponent"].Rows.Count; i++)
                {
                    if (Convert.ToInt32(dataSet.Tables["spiller"].Rows[0]["Id"]) == Convert.ToInt32(dataSet.Tables["spillercomponent"].Rows[i]["Id"]))
                    {
                        if (dataSet.Tables["spillercomponent"].Rows[i]["MId"] == null)
                            spillerComponents.Add(components[Convert.ToInt32(dataSet.Tables["spillercomponent"].Rows[i]["CId"])]);
                        else
                        {
                            mopedComponents.Add(components[Convert.ToInt32(dataSet.Tables["spillercomponent"].Rows[i]["CId"])]);
                            mopedcomponentId.Add(Convert.ToInt32(dataSet.Tables["spillercomponent"].Rows[i]["MId"]));
                        }
                    }
                }

                
                for (int i = 0; i < dataSet.Tables["spillermoped"].Rows.Count; i++)
                {
                    List<Component> currentMopedComponents = new List<Component>();

                    if (Convert.ToInt32(dataSet.Tables["spiller"].Rows[0]["Id"]) == Convert.ToInt32(dataSet.Tables["spillermoped"].Rows[i]["Id"]))
                    {
                        spillerMopeds.Add(mopeds[Convert.ToInt32(dataSet.Tables["spillermoped"].Rows[i]["MopedId"])]);

                        for (int n = 0; n < mopedcomponentId.Count; n++)
                        {
                            if (mopedcomponentId[n] == Convert.ToInt32(dataSet.Tables["spillermoped"].Rows[i]["MopedId"]))
                            {
                                currentMopedComponents.Add(mopedComponents[n]);
                            }
                        }
                        spillerMopeds.Last().Components = currentMopedComponents.ToList();

                        if (Player.Instance.ActiveMoped == null)
                        {
                            Player.Instance.ActiveMoped = spillerMopeds[0];
                        }
                    }
                }

                for (int i = 0; i < dataSet.Tables["spilleraccessory"].Rows.Count; i++)
                {
                    if (Convert.ToInt32(dataSet.Tables["spiller"].Rows[0]["Id"]) == Convert.ToInt32(dataSet.Tables["spilleraccessory"].Rows[i]["Id"]))
                    {
                        spillerAccessories.Add(accessories[Convert.ToInt32(dataSet.Tables["spilleraccessory"].Rows[i]["AId"])]);
                    }
                }

                if (spillerComponents.Count > 0)
                    Player.Instance.AvailableComps.AddRange(spillerComponents);
                if (spillerMopeds.Count > 0)
                    Player.Instance.Mopeds.AddRange(spillerMopeds);
                if (spillerAccessories.Count > 0)
                    Player.Instance.AvailableAccesories.AddRange(spillerAccessories);

                Player.Instance.Money = Convert.ToInt32(dataSet.Tables["spiller"].Rows[0]["Penge"]);
            }

            HandOverToMarket(components, mopeds, accessories);
        }

        /// <summary>
        /// sends the data about components, mopeds and accessories to market
        /// </summary>
        /// <param name="components"></param>
        /// <param name="mopeds"></param>
        /// <param name="accessories"></param>
        private void HandOverToMarket(Dictionary<int, Component> components, Dictionary<int, Moped> mopeds, Dictionary<int, Accessory> accessories)
        {
            GameManager.Market.InitializeCategories(components.Values.ToList(), accessories.Values.ToList(), mopeds.Values.ToList());
        }

        /// <summary>
        /// Saves the players mopeds, components and accessories to MopedData.db
        /// </summary>
        public void Save()
        {
            Connect(databaseConnectionLocation);
            string cmd;
            //update player

            cmd = "UPDATE Spiller SET Penge=" + Player.Instance.Money;

            ExecuteQuery(databaseConnectionLocation, cmd);


            cmd = "Delete from spillercomponent;"; //Clears spillercomponent
            cmd += "Delete from spillermoped;";    //Clears spillermoped
            cmd += "Delete from spilleraccessory;";//Clears spilleraccessory
            ExecuteQuery(databaseConnectionLocation, cmd);

            int i = 1;
            foreach (Component c in Player.Instance.AvailableComps)//adds the players components that are not assigned to one of her/his mopeds to the database
            {
                cmd = "Insert into spillercomponent Values (";
                cmd += "null,";
                cmd += c.ID + ",";
                cmd += i + ")";

                ExecuteQuery(databaseConnectionLocation, cmd);
                i++;
            }

            List<Moped> playerMopeds = new List<Moped>();
            playerMopeds.AddRange(Player.Instance.Mopeds);
            playerMopeds.Add(Player.Instance.ActiveMoped);

            foreach (Moped m in playerMopeds)//adds the players components that are  assigned to one of her/his mopeds to the database
            {
                foreach (Component c in m.Components)
                {
                    cmd = "Insert into spillercomponent Values (";
                    cmd += m.ID + ",";
                    cmd += c.ID + ",";
                    cmd += i + ")";

                    ExecuteQuery(databaseConnectionLocation, cmd);
                    i++;
                }
            }

            i = 1;
            foreach (Moped m in playerMopeds)//adds the players mopeds to the database
            {
                cmd = "Insert into spillermoped Values (";
                cmd += i + ",";
                cmd += m.ID + ")";

                ExecuteQuery(databaseConnectionLocation, cmd);
                i++;
                
            }

            List<Accessory> playerAccessories = new List<Accessory>();
            playerAccessories.AddRange(Player.Instance.Accessories);
            playerAccessories.AddRange(Player.Instance.AvailableAccesories);
            i = 1;
            foreach (Accessory a in playerAccessories)//adds the players accessories to the database
            {
                cmd = "Insert into spilleraccessory Values (";
                cmd += i + ",";
                cmd += a.ID + ")";

                ExecuteQuery(databaseConnectionLocation, cmd);
                i++;

            }

            Disconnect(databaseConnectionLocation);
        }

        /// <summary>
        /// Opens a connection gently.
        /// </summary>
		#if WINDOWS
		private void Connect(SQLiteConnection database)
		#else
		private void Connect(SqliteConnection database)
		#endif
		{
            if (database.State == ConnectionState.Closed)
            {
                database.Open();
            }
        }

        /// <summary>
        /// Closes the connection gently.
        /// </summary>
		#if WINDOWS
        private void Disconnect(SQLiteConnection database)
		#else
		private void Disconnect(SqliteConnection database)
		#endif
        {
            if (database.State == ConnectionState.Open)
            {
                database.Close();
            }
        }

        /// <summary>
        /// Disconnects all connections.
        /// </summary>
        public void DisconnectAll()
        {
            if (databaseConnectionLocation.State == ConnectionState.Open)
            {
                databaseConnectionLocation.Close();
            }
        }

        /// <summary>
        /// Converts a string of hexadecimals into a Color
        /// </summary>
        public Color HexToColor(string hex)
        {
            int r = Int32.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            int g = Int32.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            int b = Int32.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);

            return new Color(r, g, b);
        }

        /// <summary>
        /// Disposes this class manually
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Disposes this class and tjecks if its forced (manually) or not (crashed)
        /// </summary>
        /// <param name="forced">Is forced?</param>
        protected virtual void Dispose(bool forced)
        {
            dataSet.Dispose();
            databaseConnectionLocation.Dispose();
            //DisconnectAll();
        }
    }
}