﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace OldMopeds
{
    /// <summary>
    /// Unused: Accessory that the <see cref="Player"/> can wear.
    /// </summary>
    public class Accessory : GameObject
    {
        public enum AccessoryType { Helmet, Jacket };

        private AccessoryType type;
        private string name;
        private int price;
        private int id;

        public AccessoryType Type
        {
            get { return type; }
            set { type = value; }
        }

        public int Price
        {
            get { return this.price; }
            set { this.price = value; }
        }
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// Unused: Initializes a new instance of the <see cref="Accessory"/> class.
        /// </summary>
        /// <param name="name">Name of accessory.</param>
        /// <param name="type">Type of accessory.</param>
        /// <param name="price">Price of accessory.</param>
        /// <param name="id">ID of accessory.</param>
        public Accessory(string name, AccessoryType type, int price, int id)
        {
            this.id = id;
            this.name  = name;
            this.type  = type;
            this.price = price;
        }

        /// <summary>
        /// Unused: Updates <see cref="Accessory"/>.
        /// </summary>
        /// <param name="time">Current game time.</param>
        public override void Update(GameTime time)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Unused: Draws <see cref="Accessory"/>.
        /// </summary>
        /// <param name="time">Current game time.</param>
        /// <param name="sb">Current sprite batch.</param>
        public override void Draw(GameTime time, SpriteBatch sb)
        {
            throw new NotImplementedException();
        }
    }
}
