﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace OldMopeds
{
    /// <summary>
    /// Hold global variables.
    /// </summary>
    public static class GameManager
    {
        private static List<GameObject> objects;
        private static List<GameObject> guis;
        private static List<GameObject> backgrounds;
        private static Size gameSize;
        private static Random random = new Random();
        private static Camera cam;
        private static VirtualResolution renderer;

        private static Market market;
        private static Garage garage;
        private static Race race;

        public static List<GameObject> Objects
        {
            get { return GameManager.objects; }
            set { GameManager.objects = value; }
        }
        public static List<GameObject> GUIs
        {
            get { return GameManager.guis; }
            set { GameManager.guis = value; }
        }
        public static List<GameObject> Backgrounds
        {
            get { return GameManager.backgrounds; }
            set { GameManager.backgrounds = value; }
        }
        public static Size GameSize
        {
            get { return GameManager.gameSize; }
            set { GameManager.gameSize = value; }
        }
        public static Random Random
        {
            get { return random; }
            set { random = value; }
        }
        public static Camera Cam
        {
            get { return GameManager.cam; }
            set { GameManager.cam = value; }
        }
        public static VirtualResolution Renderer
        {
            get { return GameManager.renderer; }
            set { GameManager.renderer = value; }
        }

        public static Market Market
        {
            get { return GameManager.market; }
            set { GameManager.market = value; }
        }
        public static Garage Garage
        {
            get { return GameManager.garage; }
            set { GameManager.garage = value; }
        }
        public static Race Race
        {
            get { return GameManager.race; }
            set { GameManager.race = value; }
        }

        /// <summary>
        /// Initialization method for the <see cref="GameManager"/> class.
        /// </summary>
        /// <param name="loadSave"></param>
        public static void Initialize()
        {
            objects = new List<GameObject>();
            guis = new List<GameObject>();
            backgrounds = new List<GameObject>();

            // Instantiate Garage (+ Market)
            GameManager.Garage = new Garage();
            
            //Database
            DatabaseManager db = new DatabaseManager();
            db.Load();
        }

        /// <summary>
        /// Updates all <see cref="GameObject"/>s.
        /// </summary>
        /// <param name="gameTime"></param>
        public static void Update(GameTime gameTime)
        {
            // ToArray() necessary for disposing GameObjects
            foreach (var obj in objects.ToArray())
            {
                obj.Update(gameTime);
            }
        }
    }
}
