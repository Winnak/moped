﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace OldMopeds
{
    /// <summary>
    /// Contains information regarding its components, and the physics of the game.
    /// </summary>
    public class Moped : GameObject
    {
        private List<Component> components;
        
        private int baseSpeed;
        private int baseFriction;
        private int baseWeight;
        private int baseAcceleration;
        private int baseAerodynamism;

        private int maxSpeed;
        private int friction;
        private int weight;
        private int acceleration;
        private int aerodynamism;
        private int price;

        private float speed;
        private float throttle;
        private float rotation;

        private string name;
        private Vector2 position;
        private int id;

        private Component compWheelFront;
        private Component compWheelRear;
        private Component compSeat;
        private Component compEngine;
        private Component compExhaust;
        //private Component compRearAccessory;

        public List<Component> Components
        {
            get { return components; }
            set { components = value; }
        }
        public string Name 
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public Vector2 Position
        {
            get { return this.position; }
            set { this.position = value; }
        }

        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public int MaxSpeed
        {
            get { return this.maxSpeed; }
            set { this.maxSpeed = value; }
        }
        public int Friction
        {
           get { return this.friction; }
           set { this.friction = value; }
        }
        public int Weight
        {
            get { return this.weight; }
            set { this.friction = value; }
        }
        public int Acceleration
        {
            get { return this.acceleration; }
            set { this.acceleration = value; }
        }
        public int Aerodynamism
        {
            get { return this.aerodynamism; }
            set { this.aerodynamism = value; }
        }

        public int BaseSpeed
        {
            get { return this.baseSpeed; }
            set { this.baseSpeed = value; }
        }
        public int BaseFriction
        {
            get { return this.baseFriction; }
            set { this.baseFriction = value; }
        }
        public int BaseWeight
        {
            get { return this.baseWeight; }
            set { this.baseWeight = value; }
        }
        public int BaseAcceleration
        {
            get { return this.baseAcceleration; }
            set { this.baseAcceleration = value; }
        }
        public int BaseAerodynamism
        {
            get { return this.baseAerodynamism; }
            set { this.baseAerodynamism = value; }
        }

        public int Price
        {
            get {  return this.price; }
            set { this.price = value; }
        }
        public float Speed
        {
            get { return speed; }
            set { speed = value; }
        }
        public float Throttle
        {
            get { return throttle; }
            set { throttle = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Moped" /> class.
        /// </summary>
        /// <param name="name">The moped's name.</param>
        /// <param name="id">ID from database.</param>
        /// <param name="components">List of the components attached.</param>
        /// <param name="stats">Stats of the moped frame.</param>
        public Moped(string name, int id, List<Component> components, params string[] stats)
        {
            this.name = name;
            this.id = id;
            ReadParameters(stats);
            FillComponents(components);
            UpdateStats();

            this.Sprite = new Sprite(Asset.Image[this.name + "_base"]);
            Texture2D sprTex = this.Sprite.Texture;
            this.Sprite.Origin = new Vector2(sprTex.Width * 0.5f, sprTex.Height * 0.5f);

            foreach (Component comp in components)
            {
                switch (comp.Type)
                {
                    case Component.ComponentType.Wheel:
                        this.compWheelFront = comp;
                        this.compWheelRear = comp;
                        break;
                    case Component.ComponentType.Seat:
                        this.compSeat = comp;
                        break;
                    case Component.ComponentType.Engine:
                        this.compEngine = comp;
                        break;
                    case Component.ComponentType.Exhaust:
                        this.compExhaust = comp;
                        break;
                }
            }
        }

        /// <summary>
        /// Sets the stats of the bike from its components.
        /// </summary>
        /// <param name="components">List of attached components.</param>
        private void FillComponents(List<Component> components)
        {
            this.components = components;
        }

        /// <summary>
        /// Sets the stats of the frame from its paramaters constructer.
        /// </summary>
        /// <param name="stats">Constructor data.</param>
        private void ReadParameters(string[] stats)
        {
            foreach (string s in stats)
            {
                string[] stat = s.Split('=');

                if (stat[0] == "speed")
                    this.baseSpeed = Convert.ToInt32(stat[1]) * 100;

                else if (stat[0] == "friction")
                    this.baseFriction = Convert.ToInt32(stat[1]);

                else if (stat[0] == "weight")
                    this.baseWeight = Convert.ToInt32(stat[1]);

                else if (stat[0] == "acceleration")
                    this.baseAcceleration = Convert.ToInt32(stat[1]);

                else if (stat[0] == "aerodynamism")
                    this.baseAerodynamism = Convert.ToInt32(stat[1]);
            }
        }

        /// <summary>
        /// Updates <see cref="Moped"/> with all its <see cref="Component"/>s.
        /// </summary>
        /// <param name="time">Current game time.</param>
        public override void Update(GameTime time)
        {
            if (throttle < 0.05f)
            {
                throttle = 0;
                speed -= 10 * (float)time.ElapsedGameTime.TotalSeconds;
            }
            speed += throttle * this.acceleration * 0.3f;


            speed -= ((200 * this.weight) / this.friction) * 
                        (float)time.ElapsedGameTime.TotalSeconds;

            speed -= (0.0002325f * speed * speed) / this.aerodynamism;

            speed = MathHelper.Clamp(speed, 0, this.maxSpeed + (float)GameManager.Random.NextDouble() * 50);

            if (speed > this.maxSpeed)
                speed -= 2; //Fordi kameraet ikke lerper ser dette ikke så godt ud

            this.position.X += speed * (float)time.ElapsedGameTime.TotalSeconds;
            this.rotation += 0.25f * speed * (float)time.ElapsedGameTime.TotalSeconds;
        }

        /// <summary>
        /// Draws specified <see cref="Component"/>.
        /// </summary>
        /// <param name="c">Component.</param>
        /// <param name="depth">Depth value.</param>
        /// <param name="compPos">Relative position to <see cref="Moped"/>.</param>
        /// <param name="sb"><see cref="SpriteBatch"/>.</param>
        private void DrawComponent(Component c, float depth, Vector2 compPos, SpriteBatch sb)
        {
            if (c == null || c.Sprite == null)
                return;
            
            if (c.Type == Component.ComponentType.Wheel)
                sb.DrawSprite(c.Sprite, rotation, this.position + compPos, depth);
            else
                sb.DrawSprite(c.Sprite, this.position + compPos, depth);
        }

        /// <summary>
        /// Draws the <see cref="Component"/>s on the right layers.
        /// </summary>
        /// <param name="time">Current game time.</param>
        /// <param name="sb">Current sprite batch.</param>
        public override void Draw(GameTime time, SpriteBatch sb)
        {
            // Moped
            if (this.Sprite != null)
                sb.DrawSprite(this.Sprite, this.position, 0.75f);

            // Components
            DrawComponent(compWheelFront, 0.74f, new Vector2(288, 136), sb);
            DrawComponent(compWheelRear, 0.74f, new Vector2(-216, 130), sb);
            DrawComponent(compSeat, 0.76f, new Vector2(-160, -72), sb);
            DrawComponent(compEngine, 0.76f, new Vector2(28, 94), sb);
            DrawComponent(compExhaust, 0.76f, new Vector2(-108, 146), sb);
            //DrawComponent(compRearAccessory, 0.76f, sb);
        }

        public void ChangeComponent(Component newComp)
        {
            foreach (Component oldComp in components)
            {
                if (oldComp.Type == newComp.Type)
                {
                    components.Remove(oldComp);
                    components.Add(newComp);

                    UpdateStats();

                    Player.Instance.AvailableComps.Add(oldComp);
                    Player.Instance.AvailableComps.Remove(newComp);

                    break;
                }
            }

            AssignComponent(newComp);
        }

        /// <summary>
        /// Attaches a <see cref="Component"/> to the active moped.
        /// </summary>
        /// <param name="comp">The <see cref="Component"/>.</param>
        private void AssignComponent(Component comp)
        {
            switch (comp.Type)
            {
                case Component.ComponentType.Wheel:
                    compWheelFront = comp;
                    compWheelRear = comp;
                    break;
                case Component.ComponentType.Seat:
                    compSeat = comp;
                    break;
                case Component.ComponentType.Engine:
                    compEngine = comp;
                    break;
                case Component.ComponentType.Exhaust:
                    compExhaust = comp;
                    break;
            }
        }

        /// <summary>
        /// Clears stats and gets the new stats.
        /// </summary>
        private void UpdateStats()
        {
            ResetStats();

            foreach (Component comp in components)
            {
                maxSpeed     += comp.Speed;
                friction     += comp.Friction;
                weight       += comp.Weight;
                acceleration += comp.Acceleration;
                aerodynamism += comp.Aerodynamism;
            }
        }

        /// <summary>
        /// Clears stats.
        /// </summary>
        private void ResetStats()
        {
            maxSpeed          = baseSpeed;
            friction          = baseFriction;
            acceleration      = baseAcceleration;
            aerodynamism      = baseAerodynamism;
            weight            = baseWeight;
        }

        /// <summary>
        /// Prints out the bikes stats.
        /// </summary>
        /// <returns>Moped's stats in point form.</returns>
        public override string ToString()
        {
            return "Speed: " + maxSpeed + "\nFriction: " + friction + "\nWeight: "
              + weight + "\nAcceleration: " + acceleration + "\nAerodynamsim: " + aerodynamism;
        }
    }
}
