﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace OldMopeds
{
    /// <summary>
    /// Creates, saves and loads a text log of selected events. 
    /// </summary>
    public class Logger : IDisposable
    {
        private const string FilePath = "Log";
        private string filename;
        private FileStream file;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logger"/> class.
        /// </summary>
        /// <param name="frequency">Frequency of events.</param>
        public Logger(double frequency)
        {
            if (!Directory.Exists(FilePath + "/"))
            {
                Directory.CreateDirectory(FilePath);
            }
#if WINDOWS
            this.filename = FilePath + "\\" + DateTime.UtcNow.ToString() + ".log";
			this.filename = this.filename.Replace(':', '-');
#else
            this.filename = FilePath + "/" + DateTime.UtcNow.ToString() + ".log";
			this.filename = this.filename.Replace(':', '.');
			this.filename = this.filename.Replace('/', '-');
#endif
            this.file = File.Create(filename);

            this.Log(frequency.ToString() + '&');
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="Logger" /> class.
        /// </summary>
        ~Logger()
        {
            Dispose(false);
        }

        /// <summary>
        /// Logs an event and saves to file.
        /// </summary>
        /// <param name="data">The event.</param>
        public void Log (string data)
        {
            var buffer = Encoding.UTF8.GetBytes(data);

            for (int i = 0; i < buffer.Length; i++)
                this.file.WriteByte(buffer[i]);
        }

        /// <summary>
        /// Loads a log and interpts the data.
        /// </summary>
        /// <param name="frequency">Frequency of the log.</param>
        /// <param name="data">Data values.</param>
        /// <param name="filename">File name or if non: the latest file.</param>
        /// <returns>Wether the log was loaded succesfully or not.</returns>
        public static bool LoadData(ref double frequency,  
                                    ref float[] data, 
                                    string filename = "Latest")
        {
            string wdata;

            if (!Directory.Exists(FilePath))
                return false;

            if (filename == "Latest")
            {
                string[] files = Directory.GetFiles(FilePath);
                DateTime latest;

                //Return if there is no files
                if (files.Length == 0)
                    return false;

                latest = File.GetCreationTimeUtc(files[0]);

                string file = files[0];

                //return file with latest creation date
                foreach (var item in files)
                {
                    var tem = File.GetCreationTimeUtc(item);
                    if (tem > latest)
                    {
                        latest = tem;
                        file = item;
                    }
                }

                wdata = File.ReadAllText(file);

                //return if file is empty
                if (wdata.Length <= 0)
                    return false;
            }
            else
            {
                //read file by file name
                if (File.Exists(filename))
                    wdata = File.ReadAllText(filename);
                else 
                    return false;
            }

            //Seperate header
            string[] pass = wdata.Split('&');
            frequency = Convert.ToDouble(pass[0]);
            wdata     = pass[1];

            //Convert values
            string[] entries = wdata.Split('|');
            float[] final  = new float[entries.Length - 1];
            for (int i = 0; i < entries.Length - 1; i++)
            {
                final[i] = (float)Convert.ToDouble(entries[i]);
            }
            
            data = final;

            return true;
        }

        /// <summary>
        /// Disposes the Logger.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes the Logger.
        /// </summary>
        /// <param name="forced">Was dipose forced?</param>
        private void Dispose(bool forced)
        {
            this.file.Close();

            if (!forced)
                File.Delete(this.filename);

            file.Dispose();
        }
    }
}
