﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Text;

namespace OldMopeds
{
    /// <summary>
    /// Opponent for the drag race.
    /// </summary>
    public class Ghost : GameObject
    {
        Vector2 position;
        private float speed;

        private double frequency;
        private float[] data;
        private int dataIndex;

        private double counter;

        /// <summary>
        /// Initializes a new instance of the <see cref="Ghost"/> class.
        /// </summary>
        /// <param name="freq">Frequency of data.</param>
        /// <param name="data">Data values.</param>
        /// <param name="position">Initial Position.</param>
        public Ghost(double freq, float[] data, Vector2 position)
        {
            this.Sprite = new Sprite(Asset.Image["Yamaha-fs1"]);
            this.Sprite.Color = new Color(230, 250, 255, 128);

            this.position = position + new Vector2(-100, -100);

            this.frequency = freq;
            this.data      = data;
            this.dataIndex = 0;

            this.counter = 0;
        }

        /// <summary>
        /// Updates the speed to match with the log.
        /// </summary>
        /// <param name="time">Current game time.</param>
        public override void Update(GameTime time)
        {
            counter += time.ElapsedGameTime.TotalSeconds;
            if (counter > this.frequency)
            {
                counter = 0;

                if (dataIndex < data.Length)
                {
                    this.speed = (data[dataIndex] - position.X);
                    dataIndex++;
                }
            }

            this.position.X += speed 
                        * (float)(time.ElapsedGameTime.TotalSeconds/frequency);
        }

        /// <summary>
        /// Draws the <see cref="Ghost"/> object.
        /// </summary>
        /// <param name="time">Current game time.</param>
        /// <param name="sb">Current sprite batch.</param>
        public override void Draw(GameTime time, SpriteBatch sb)
        {
            sb.DrawSprite(this.Sprite, position, 0.9f);
        }
    }
}
