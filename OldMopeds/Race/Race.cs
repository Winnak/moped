﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace OldMopeds
{
    /// <summary>
    /// Drag race scene.
    /// </summary>
    public class Race : GameObject
    {
        //Moped
        private Moped activeMoped;

        //backgrounds
        private BackgroundElement backgroundSun;
        private BackgroundElement backgroundGrass;
        private BackgroundElement backgroundRoad1;
        private BackgroundElement backgroundRoad2;
        private BackgroundElement backgroundRoad3;
        private BackgroundElement goal;

        //Inteface
        private Slider slider;

        //Logging
        private Logger logger;
        private double logCounter;
        private const double LogFrequency = 0.5;
        
        //Ghosting
        private Ghost ghost;
        private double ghostUpdate;
        private float[] ghostData;

        /// <summary>
        /// Initializes a new instance of the <see cref="Race"/> class.
        /// </summary>
        public Race()
        {
            //Load backgrounds
            backgroundSun = new BackgroundElement("Background_Race_02", Point.Zero, 0.1f);
            backgroundGrass = new BackgroundElement("Background_Race_03",
                new Vector2(-600, 750), 0.20f);
            backgroundRoad1 = new BackgroundElement("Background_Race_04",
                new Vector2(-600, 550), 0.21f);
            backgroundRoad2 = new BackgroundElement("Background_Race_05",
                new Vector2(-600, 800), 0.22f);
            backgroundRoad3 = new BackgroundElement("Background_Race_06",
                new Vector2(-600, 800), 0.23f);
            goal = new BackgroundElement("goal", new Vector2(20000, 0), 0.85f, false);
            backgroundRoad3.Parralax = 25;

            slider = new Slider(new Vector2(10, 100));


            //Load Moped
            this.activeMoped = Player.Instance.ActiveMoped;
            if (this.activeMoped == null)
            {
                this.activeMoped =
                    new Moped("Yamaha-fs1", 2000, new List<Component>(), "speed=5", "acceleration=5", "aerodynamism=5");
            }
            else
                GameManager.Objects.Add(this.activeMoped);

            activeMoped.Position = GameManager.Cam.Position + Vector2.One * 950;

            if (Logger.LoadData(ref ghostUpdate, ref ghostData))
            {
                ghost = new Ghost(ghostUpdate, ghostData, activeMoped.Position);
            }

            logger = new Logger(LogFrequency); //VIGTIG dette skal stå efter.
        }

        /// <summary>
        /// Logs player position, moves camera, parallaxes backgrounds and a 
        /// little game logic.
        /// </summary>
        /// <param name="time">Current game time.</param>
        public override void Update(GameTime time)
        {
            //Log game
            logCounter += time.ElapsedGameTime.TotalSeconds;
            if (logCounter > LogFrequency)
            {
                logger.Log(activeMoped.Position.X.ToString() + '|');

                logCounter = 0;   
            }

            //Adjust throttle
            activeMoped.Throttle = slider.Level;

            //Paralax backgroudns
            backgroundRoad3.Speed = activeMoped.Speed;
            goal.Speed = 0;

            // Set camera position
            if (activeMoped.Speed < activeMoped.MaxSpeed * 0.99f)
            {
                GameManager.Cam.Position = activeMoped.Position + 
                    new Vector2(
                       400 - (activeMoped.MaxSpeed - activeMoped.Speed) * 0.6f, 
                      -510 + (activeMoped.MaxSpeed - activeMoped.Speed) * 0.8f);
            }
            else
            {
                GameManager.Cam.Position = activeMoped.Position + new Vector2(388, -505.5f);
            }

            // Set camera Zoom
            GameManager.Cam.Zoom = 1 - (float)activeMoped.Speed / (float)activeMoped.MaxSpeed;

            if (activeMoped.Position.X > 20000)
            {
                this.Dispose();
            }
        }

        /// <summary>
        /// Unused: See <see cref="Race"/> class.
        /// </summary>
        /// <param name="time">Current game time.</param>
        /// <param name="sb">Current sprite batch.</param>
        public override void Draw(GameTime time, SpriteBatch sb)
        {

        }

        /// <summary>
        /// Disposes the <see cref="Race"/> object.
        /// </summary>
        /// <param name="forced">Was dispose forced?</param>
        protected override void Dispose(bool forced)
        {
            GameManager.Objects.Clear();
            GameManager.GUIs.Clear();
            GameManager.Backgrounds.Clear();
            slider.Dispose();
            backgroundSun.Dispose();
            backgroundGrass.Dispose();
            backgroundRoad1.Dispose();
            backgroundRoad2.Dispose();
            backgroundRoad3.Dispose();
            goal.Dispose();
            logger.Dispose();

            if (ghost != null)
            {
                ghost.Dispose();   
            }

            if (forced)
            {
                GameManager.Cam.Zoom = 1;
                GameManager.Cam.Position = Vector2.Zero;
                GameManager.Garage = new Garage();
                
                DatabaseManager test = new DatabaseManager();
                test.Load();
            }

            base.Dispose(forced);
        }
    }
}
