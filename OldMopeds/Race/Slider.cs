﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace OldMopeds
{
    /// <summary>
    /// Interface element for controlling the throttle when racing
    /// </summary>
    public class Slider : GameObject
    {
        private Rectangle back;

        private Sprite slider;
        private Vector2 sliderPos;

        private float level = 0;

        /// <summary>
        /// Returns a percentage of throttle.
        /// </summary>
        public float Level
        {
            get { return MathHelper.Clamp(level, 0, 1); }
            set { level = MathHelper.Clamp(value, 0, 1); }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Slider"/> class.
        /// </summary>
        /// <param name="position">Start position</param>
        public Slider(Vector2 position)
        {
            this.Sprite = new Sprite(Asset.Image["SliderBack"]);
            this.slider = new Sprite(Asset.Image["Slider"]);

            this.back      = new Rectangle().Create(position, this.Sprite.Texture.Size());
            this.sliderPos = position;

            DrawInGUI = true;
        }

        /// <summary>
        /// Checks the current position of the sliding knob.
        /// </summary>
        /// <param name="time">Current game time.</param>
        public override void Update(GameTime time)
        {
            if (Input.CurrentTouchState == Input.TouchState.Begin || 
                Input.CurrentTouchState == Input.TouchState.Moved)
            {
                if (this.back.Contains(Input.MousePosition))
                {
                    var dy = Input.MousePosition.Y - this.back.Position().Y;

                    level = dy / 500;
                }
            }

            sliderPos.Y = level * 500 + back.Y - slider.Texture.Height/2;
            sliderPos.Y = MathHelper.Clamp(sliderPos.Y, 
                back.Y + 2, back.Y + back.Height - slider.Texture.Height - 5);
        }

        /// <summary>
        /// Draws the <see cref="Slider"/>
        /// </summary>
        /// <param name="time">Current game time.</param>
        /// <param name="sb">Current sprite batch.</param>
        public override void Draw(GameTime time, SpriteBatch sb)
        {
            sb.DrawSprite(this.Sprite, back.Position(),  0.98f);
            sb.DrawSprite(this.slider, sliderPos, 0.99f);
        }
    }
}
