﻿using System;
//using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace OldMopeds
{
    /// <summary>
    /// Draws images in the background.
    /// </summary>
    public class BackgroundElement : GameObject
    {
        bool inGui;
        bool loop;
        float depth;
        int parralax = 0;
        float speed = 0;
        Rectangle rect;
        Rectangle rect2;

        public int Parralax
        {
            get { return parralax; }
            set { parralax = value; }
        }
        public float Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        /// <summary>
        /// <para>Initializes a new instance of the <see cref="BackgroundElement"/> class.</para>
        /// <para>Constructer for moving backgrounds</para>
        /// </summary>
        /// <param name="filePath">Path to texture image.</param>
        /// <param name="worldPos">World position.</param>
        /// <param name="depth">depth layer.</param>
        /// <param name="loop">Looping background.</param>
        public BackgroundElement(string filePath, Vector2 worldPos, float depth, bool loop = true)
        {
            this.Sprite     = new Sprite(Asset.Image[filePath]);
            this.rect       = this.Sprite.SrcRectangle;
            this.rect.X     = (int)worldPos.X;
            this.rect.Y     = (int)worldPos.Y;
            this.rect2      = rect.ChangePosition(Vector2.UnitX * rect.Width);
            this.depth      = depth;
            this.inGui      = false;

            this.loop       = loop;
        }
        /// <summary>
        /// <para>Initializes a new instance of the <see cref="BackgroundElement"/> class.</para>
        /// <para>Constructer for non moving backgrounds</para>
        /// </summary>
        /// <param name="filePath">Path to texture image.</param>
        /// <param name="screenPos">Position on screen.</param>
        /// <param name="depth">Depth layer.</param>
        public BackgroundElement(string filePath, Point screenPos, float depth)
        {
            this.Sprite     = new Sprite(Asset.Image[filePath]);
            this.rect       = this.Sprite.SrcRectangle;
            this.depth      = depth;
            this.inGui      = true;

            this.loop = false;

            GameManager.Objects.Remove(this);
            GameManager.Backgrounds.Add(this);
        }

        /// <summary>
        /// Updates positions and loops backgrounds.
        /// </summary>
        /// <param name="time">Current game time.</param>
        public override void Update(GameTime time)
        {
            // The bad way to parallaxing
            speed *= (float)(parralax * time.ElapsedGameTime.TotalSeconds);
            rect.X  -= (int)Math.Min(speed, parralax);
            rect2.X -= (int)Math.Min(speed, parralax);

            if (loop)
            {
                if (rect.X + rect.Width < GameManager.Cam.ToVirtual(Point.Zero).X)
                {
                    this.rect.X = this.rect2.X + rect2.Width;
                }

                if (rect2.X + rect2.Width < GameManager.Cam.ToVirtual(Point.Zero).X)
                {
                    this.rect2.X = this.rect.X + rect.Width;
                }
            }
        }

        /// <summary>
        /// Draws the background onto the scene.
        /// </summary>
        /// <param name="time">Current game time.</param>
        /// <param name="sb">Current sprite batch.</param>
        public override void Draw(GameTime time, SpriteBatch sb)
        {
            sb.DrawSprite(Sprite, rect.Position(), depth);

            if (!inGui && loop)
                sb.DrawSprite(Sprite, rect2.Position(), depth);
        }
    }
}
