﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace OldMopeds
{
    /// <summary>
    /// An ingame market where the Player can buy new components, 
    /// mopeds and accessories and change allready owned ones.
    /// </summary>
    public class Market : GameObject
    {
        private List<Component> components = new List<Component>();
        private List<Accessory> accessories = new List<Accessory>();
        private List<Moped> mopeds = new List<Moped>();

        private InterfaceScroll categoryScroll;
        private InterfaceScroll itemsScroll;
        private List<GameObject> categoryItems = new List<GameObject>();

        public List<Component> Components
        {
            get
            {
                return components;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Market"/> class.
        /// </summary>
        public Market()
        {
            // Background
            Texture2D tex = Asset.Image["Background_Market"];
            Size size = new Size(tex.Width, tex.Height);
            Vector2 pos = new Vector2(-GameManager.GameSize.Width * 1.5f, -GameManager.GameSize.Height * 0.5f);
            new InterfacePicture(tex, pos, size, Color.White, 0.0f);
        }

        /// <summary>
        /// Creates InterfaceScroll categories with InterfaceButtons from given 
        /// lists of components, accessories, and mopeds.
        /// </summary>
        public void InitializeCategories(List<Component> components, List<Accessory> accessories, List<Moped> mopeds)
        {
            // Read lists
            this.components.AddRange(components);
            this.accessories.AddRange(accessories);
            this.mopeds.AddRange(mopeds);

            // Create category InterfaceScroll
            categoryScroll = new InterfaceScroll(
                new Vector2(-GameManager.GameSize.Width * 1.5f, -GameManager.GameSize.Height * 0.5f), 
                new Size(170, 175 * 
                    (Enum.GetNames(typeof(Component.ComponentType)).Length + 
                     Enum.GetNames(typeof(Accessory.AccessoryType)).Length + 1) + 40), 
                Color.Transparent, 0.0f);

            // Get all category types
            List<string> categoryNames = new List<string>();
            categoryNames.Add("Moped");
            categoryNames.AddRange(Enum.GetNames(typeof(Component.ComponentType)));
            categoryNames.AddRange(Enum.GetNames(typeof(Accessory.AccessoryType)));

            // Add category buttons
            for (int i = 0; i < categoryNames.Count; i++)
            {
                InterfaceButton iBtn = new InterfaceButton(
                    new Vector2(0, i * 175), new Size(170),
                    new Color(Color.Black, 0.5f),
                    0.1f, false, categoryScroll);

                iBtn.AddFunction(SwitchCategory, categoryNames[i]);
                
                InterfacePicture iPic = new InterfacePicture(
                    "Category" + categoryNames[i], Vector2.Zero,
                    new Size(170), Color.White, 0.2f, false, iBtn);
            }

            SwitchCategory("Moped");
        }

        /// <summary>
        /// Disposes the previous interface elements in the active button lists and 
        /// repopulates with the appropriate items.
        /// </summary>
        /// <param name="category">Category to switch to.</param>
        public void SwitchCategory(string category)
        {
            // Recreate itemsScroll
            if (itemsScroll != null)
                itemsScroll.Dispose();

            itemsScroll = new InterfaceScroll(
                    new Vector2(-GameManager.GameSize.Width * 1.5f + 175, -GameManager.GameSize.Height * 0.5f),
                    new Size(GameManager.GameSize.Width - 175, 40), // 40 = bottomBar height
                    Color.Transparent, 0.0f);

            // Fill categoryItems
            categoryItems.Clear();

            switch (category)
            {
                case "Moped":
                    categoryItems.AddRange(mopeds);
                    break;

                case "Wheel":
                case "Seat":
                case "Engine":
                case "Exhaust":
                case "RearAccessory":
                    foreach (Component comp in components)
                    {
                        if (comp.Type.ToString() == category)
                            categoryItems.Add(comp);
                    }
                    break;

                case "Helmet":
                case "Jacket":
                    foreach (Accessory acc in accessories)
                    {
                        if (acc.Type.ToString() == category)
                            categoryItems.Add(acc);
                    }
                    break;
            }

            itemsScroll.Size += new Size(0, 175 * categoryItems.Count);
            
            // Create InterfaceButtons from categoryItems
            for (int i = 0; i < categoryItems.Count; i++)
            {
                InterfaceButton btn = new InterfaceButton(
                        new Vector2(0, 175 * i), new Size(itemsScroll.Size.Width, 170),
                        new Color(Color.Black, 0.25f), 0.5f, false, itemsScroll);

                string name;
                string price = "OWNED";

                if (categoryItems[i] is Moped)
                {
                    btn.AddFunction(BuyMoped, (categoryItems[i] as Moped).Name);

                    name = (categoryItems[i] as Moped).Name;

                    if (!Player.Instance.Mopeds.Contains(categoryItems[i]))
                        price = (categoryItems[i] as Moped).Price.ToString() + ",-";
                }
                else if (categoryItems[i] is Component)
                {
                    btn.AddFunction(BuyComponent, (categoryItems[i] as Component).Name);

                    name = (categoryItems[i] as Component).Name;

                    if (!Player.Instance.AvailableComps.Contains(categoryItems[i]) && !Player.Instance.ActiveMoped.Components.Contains(categoryItems[i]))
                        price = (categoryItems[i] as Component).Price.ToString() + ",-";
                }
                else
                {
                    btn.AddFunction(BuyAccessory, (categoryItems[i] as Accessory).Name);

                    name = (categoryItems[i] as Accessory).Name;

                    if (!Player.Instance.AvailableAccesories.Contains(categoryItems[i]) && !Player.Instance.Accessories.Contains(categoryItems[i]))
                        price = (categoryItems[i] as Accessory).Price.ToString() + ",-";
                }

                Texture2D tex = Asset.Image[name];
                float xscale = 170 / (float)tex.Width;
                Size size = new Size(170, (int)(tex.Height * xscale));
                Vector2 pos = new Vector2(0, 170 * 0.5f - size.Height * 0.5f);
                new InterfacePicture(tex, pos, size, Color.White, 0.6f, false, btn);

                new InterfaceText(price.ToString(), Asset.Font["WideLatin36"], InterfaceText.Anchors.BottomRight,
                    new Vector2(btn.Size.Width, btn.Size.Height), Color.DarkRed, 0.6f, false, btn);

                name = name.Replace('_', ' ');

                new InterfaceText(name, Asset.Font["WideLatin36"], InterfaceText.Anchors.TopRight,
                    new Vector2(btn.Size.Width, 0), Color.White, 0.6f, false, btn);
            }
        }

        /// <summary>
        /// Method that takes care of finding and transfering a specific 
        /// component to the player, and subtract money from the player 
        /// according to the cost of the component. However if the player 
        /// allready owns the component this changes the active component 
        /// with the selected component.
        /// </summary>
        /// <param name="name">Name of component.</param>
        public void BuyComponent(string name)
        {
            Component component = components.Find(t => t.Name == name);

            if (component.Price < Player.Instance.Money && !Player.Instance.AvailableComps.Contains(component) && !Player.Instance.ActiveMoped.Components.Contains(component))
            {
                Player.Instance.Money -= component.Price;
                Player.Instance.AvailableComps.Add(component);
                GameManager.Garage.ChangeScene(false);

            }

            if (Player.Instance.AvailableComps.Contains(component))
            {
                Player.Instance.ActiveMoped.ChangeComponent(component);
                GameManager.Garage.ChangeScene(false);
            }

            SwitchCategory(component.Type.ToString());
        }

        /// <summary>
        /// Method that takes care of finding and transfering a specific accessory to the player and subtract money from the player according to the cost of the accessory
        /// However if the player allready owns the accessory this changes the active accessory with the selected accessory.
        /// </summary>
        /// <param name="name">Name of accessory.</param>
        public void BuyAccessory(string name)
        {
            Accessory accessory = accessories.Find(t => t.Name == name);

            if (accessory.Price < Player.Instance.Money && !Player.Instance.AvailableAccesories.Contains(accessory) && !Player.Instance.Accessories.Contains(accessory))
            {
                Player.Instance.Money -= accessory.Price;
                Player.Instance.AvailableAccesories.Add(accessory);
                GameManager.Garage.ChangeScene(false);
            }

            if (Player.Instance.AvailableAccesories.Contains(accessory))
            {
                Player.Instance.ChangeAccesory(accessory);
                GameManager.Garage.ChangeScene(false);
            }

            SwitchCategory(accessory.Type.ToString());
            GameManager.Garage.ChangeScene(false);
        }

        /// <summary>
        /// Method that takes care of finding and transfering a specific moped to the player and subtract money from the player according to the cost of the moped
        /// However if the player allready owns the moped this changes the active moped with the selected moped.
        /// </summary>
        /// <param name="name">Name of moped.</param>
        public void BuyMoped(string name)
        {
            Moped moped = mopeds.Find(t => t.Name == name);

            if (moped.Price < Player.Instance.Money && !Player.Instance.Mopeds.Contains(moped) && Player.Instance.ActiveMoped != moped)
            {
                Player.Instance.Money -= moped.Price;
                Player.Instance.Mopeds.Add(moped);
            }

            if (Player.Instance.ActiveMoped != moped)
                Player.Instance.ActiveMoped = moped;

            SwitchCategory("Moped");
            GameManager.Garage.ChangeScene(false);
        }

        /// <summary>
        /// Unused: Draws the market.
        /// </summary>
        /// <param name="time">Current game time.</param>
        /// <param name="sb">Current sprite batch.</param>
        public override void Draw(GameTime time, SpriteBatch sb)
        {
        }

        /// <summary>
        /// Unused: Updates the market.
        /// </summary>
        /// <param name="gameTime">Current game time.</param>
        public override void Update(GameTime gameTime)
        {
        }
    }
}
