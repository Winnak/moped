﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Text;

namespace OldMopeds
{
    /// <summary>
    /// A Component that mopeds can use, which boost a mopeds stats with its own.
    /// </summary>
    public class Component : GameObject
    {
        public enum ComponentType { Wheel, Seat, Engine, Exhaust, RearAccessory };

        private ComponentType type;
        private string name;
        private int id;
        private int speed;
        private int friction;
        private int weight;
        private int acceleration;
        private int aerodynamism;
        private int price;

        public ComponentType Type
        {
            get { return this.type; }
            set { this.type = value; }
        }
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }
        public int Speed
        { 
            get { return this.speed; }
            set { this.speed = value; }
        }
        public int Friction
        {
            get { return this.friction; }
            set { this.friction = value; }
        }
        public int Weight
        {
            get { return this.weight; }
            set { this.weight = value; }
        }
        public int Acceleration
        {
            get { return this.acceleration; }
            set { this.acceleration = value; }
        }
        public int Aerodynamism
        {
            get { return this.aerodynamism; }
            set { this.aerodynamism = value; }
        }
        public int Price
        {
            get { return this.price; }
            set { this.price = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Component"/> class.
        /// </summary>
        /// <param name="name">Name of compenent.</param>
        /// <param name="componentType">The Type.</param>
        /// <param name="id">ID used by database.</param>
        /// <param name="price">The price.</param>
        /// <param name="stats">Component stats.</param>
        public Component(string name, ComponentType componentType, int id, int price, params string[] stats)
        {
            this.name = name;
            this.type = componentType;
            this.price = price;
            this.id = id;
            ReadParametersComp(stats);

            this.Sprite = new Sprite(Asset.Image[name]);
            Texture2D sprTex = this.Sprite.Texture;
            this.Sprite.Origin = new Vector2(sprTex.Width * 0.5f, sprTex.Height * 0.5f);
        }

        /// <summary>
        /// Method that reads the array of stats and assigns them to the corrosponding stats.
        /// </summary>
        /// <param name="stats">Array of stats.</param>
        public void ReadParametersComp(string[] stats)
        {
            foreach (string s in stats)
            {
                string[] stat = s.Split('=');

                if (stat[0] == "speed")
                    speed = Convert.ToInt32(stat[1]);
                else if (stat[0] == "friction")
                    friction = Convert.ToInt32(stat[1]);
                else if (stat[0] == "weight")
                    weight = Convert.ToInt32(stat[1]);
                else if (stat[0] == "acceleration")
                    acceleration = Convert.ToInt32(stat[1]);
                else if (stat[0] == "aerodynamism")
                    aerodynamism = Convert.ToInt32(stat[1]);
            }
        }

        /// <summary>
        /// Unused: Update method.
        /// </summary>
        /// <param name="time">Current game time.</param>
        public override void Update(GameTime time)
        {
        }

        /// <summary>
        /// Unused: Draw method.
        /// </summary>
        /// <param name="time">Current game time.</param>
        /// <param name="sb">Current sprite batch.</param>
        public override void Draw(GameTime time, SpriteBatch sb)
        {
        }
    }
}
