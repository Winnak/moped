﻿using Microsoft.Xna.Framework;

namespace OldMopeds
{
    /// <summary>
    /// Converts a spirtesheet (<seealso cref="Sprite"/>) into an animation.
    /// </summary>
    public class Animation
    {
        public Rectangle[] rectangles;

        public int frameIndex = 0;
        public float timeElapsed = 0;
        public float animFps = 10;
        public Vector2 origin = Vector2.Zero;
        public bool loops = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="Animation"/> class.
        /// </summary>
        /// <param name="firstFrame">First frame of the animation.</param>
        /// <param name="frames">Number of frames in the animation.</param>
        /// <param name="animSpeed">Frames per second.</param>
        /// <param name="origin">Origin of frame.</param>
        /// <param name="loops">Loops animation.</param>
        public Animation(Rectangle firstFrame, int frames, float animSpeed, Vector2 origin, bool loops = true)
        {
            this.rectangles = new Rectangle[frames];

            for (int i = 0; i < frames; i++)
            {
                this.rectangles[i] = new Rectangle(firstFrame.X + firstFrame.Width * i, firstFrame.Y, firstFrame.Width, firstFrame.Height);
            }

            this.animFps = animSpeed;
            this.origin = origin;
            this.loops = loops;
        }

        /// <summary>
        /// Animates spritesheet.
        /// </summary>
        /// <param name="gameTime">Current game time.</param>
        /// <returns>Current frame.</returns>
        public Rectangle Animate(GameTime gameTime)
        {
            this.timeElapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;

            this.frameIndex = (int)(this.timeElapsed * this.animFps);

            if (this.frameIndex > this.rectangles.Length - 1)
            {
                if (this.loops)
                {
                    this.timeElapsed = 0;
                    this.frameIndex = 0;
                }
                else
                {
                    this.frameIndex = this.rectangles.Length - 1;
                }
            }

            return this.rectangles[this.frameIndex];
        }
    }
}