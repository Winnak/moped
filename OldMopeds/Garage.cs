﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace OldMopeds
{
    /// <summary>
    /// The Players Garage where the player can change components and start a new race.
    /// the Garage displays the Players active Moped with the components that the player has choosen.
    /// The player can also swipe to the left and access the Market.
    /// </summary>
    public class Garage : GameObject
    {
        private InterfacePicture background;
        private InterfaceElement bottomBar;
        private InterfacePicture bottomBarGarage;
        private InterfacePicture bottomBarMarket;
        private InterfaceText bottomBarCash;

        private Moped moped;
        private InterfaceElement iComponentRings;

        private Vector2 camTargetPos = Vector2.Zero;

        /// <summary>
        /// Initializes a new instance of the <see cref="Garage"/> class.
        /// </summary>
        public Garage()
        {
            GameManager.Market = new Market();
            SetupGarage();
        }

        /// <summary>
        /// Setup the garage by added a background and a Gui element that changes wether the player is in the Garage or the market
        /// Also Adds a Race Button that starts op the race
        /// </summary>
        private void SetupGarage()
        {
            Texture2D tex;
            Size size;
            Vector2 pos;

            // Background
            tex = Asset.Image["Background_Garage"];
            size = new Size(tex.Width, tex.Height);
            pos = new Vector2(-GameManager.GameSize.Width * 0.5f, -GameManager.GameSize.Height * 0.5f);
            background = new InterfacePicture(tex, pos, size, Color.White, 0.0f);

            // BottomBar
            pos = new Vector2(0, GameManager.GameSize.Height - 40);
            size = new Size(GameManager.GameSize.Width, 40);
            bottomBar = new InterfaceElement(pos, size, Color.Black, 0, true);

            tex = Asset.Image["gui_bottombar_boxempty"];
            size = new Size(tex.Width, tex.Height);
            pos = new Vector2(bottomBar.Size.Width * 0.5f - tex.Width, bottomBar.Size.Height * 0.5f - tex.Height * 0.5f);
            bottomBarMarket = new InterfacePicture(tex, pos, size, Color.White, 0.1f, true, bottomBar);

            tex = Asset.Image["gui_bottombar_boxfilled"];
            pos += new Vector2(tex.Width, 0);
            bottomBarGarage = new InterfacePicture(tex, pos, size, Color.White, 0.1f, true, bottomBar);

            pos = new Vector2(GameManager.GameSize.Width, 24);
            bottomBarCash = new InterfaceText("$$$", Asset.Font["WideLatin36"], InterfaceText.Anchors.MiddleRight, pos, Color.Green, 0.15f, true, bottomBar);

            // Race button
            pos = GameManager.Cam.ToVirtual(new Point(1024, 80));
            size = new Size(80, 80);
            InterfaceButton iBtnRace = new InterfaceButton(pos, size, "gui_race-button", Color.White, 0.5f);
            iBtnRace.AddFunction(StartRace);

            // Moped
            //moped = new Moped("Yamaha-fs1", new Vector2(-35, -48));

            // Component rings
            IComponentRings();
        }

        /// <summary>
        /// Creates rings around key attachment points on the moped.
        /// </summary>
        private void IComponentRings()
        {
            Texture2D tex;
            Size size;
            Vector2 pos;
            
            InterfaceButton iBtn;
            tex = Asset.Image["gui_component_ring"];
            size = new Size(tex.Width, tex.Height);
            Color color = new Color(Color.Gray, 0.66f);
            float depth = 0.8f;

            iComponentRings = new InterfaceElement(Vector2.Zero, Color.Transparent, 0);

            moped = Player.Instance.ActiveMoped;
            
            pos = new Vector2(288, 136) - new Vector2(tex.Width * 0.5f, tex.Height * 0.5f);
            iBtn = new InterfaceButton(pos, size, tex, color, depth, false, iComponentRings);
            iBtn.AddFunction(IComponentRingClick, (int)Component.ComponentType.Wheel);

            pos = new Vector2(-100, -64) - new Vector2(tex.Width * 0.5f, tex.Height * 0.5f);
            iBtn = new InterfaceButton(pos, size, tex, color, depth, false, iComponentRings);
            iBtn.AddFunction(IComponentRingClick, (int)Component.ComponentType.Seat);

            pos = new Vector2(10, 110) - new Vector2(tex.Width * 0.5f, tex.Height * 0.5f);
            iBtn = new InterfaceButton(pos, size, tex, color, depth, false, iComponentRings);
            iBtn.AddFunction(IComponentRingClick, (int)Component.ComponentType.Engine);

            pos = new Vector2(-175, 155) - new Vector2(tex.Width * 0.5f, tex.Height * 0.5f);
            iBtn = new InterfaceButton(pos, size, tex, color, depth, false, iComponentRings);
            iBtn.AddFunction(IComponentRingClick, (int)Component.ComponentType.Exhaust);

            tex = Asset.Image["gui_component_ring_big"];
            size = new Size(tex.Width, tex.Height);

            pos = new Vector2(150, -64) - new Vector2(tex.Width * 0.5f, tex.Height * 0.5f);
            iBtn = new InterfaceButton(pos, size, tex, color, depth, false, iComponentRings);
            iBtn.AddFunction(IComponentRingClick, -1);
        }

        /// <summary>
        /// Action for IComponentRings.
        /// </summary>
        /// <param name="componentInt">Ring ID.</param>
        private void IComponentRingClick(int componentInt)
        {
            ChangeScene(true);

            if (componentInt >= 0)
                GameManager.Market.SwitchCategory(((Component.ComponentType)componentInt).ToString());
            else if (componentInt == -1)
                GameManager.Market.SwitchCategory("Moped");
        }

        /// <summary>
        /// Method that disposes the objects in GameManager.Objects and starts a new <see cref="Race"/>.
        /// </summary>
        private void StartRace()
        {
            foreach (GameObject obj in GameManager.Objects.ToArray())
            {
                obj.Dispose();
            }

            new Race();
        }

        /// <summary>
        /// Checks distance and direction of a Swipe and changes scene.
        /// </summary>
        /// <param name="time">Current game time.</param>
        public override void Update(GameTime time)
        {
            double distance = 0;
            Input.Direction inputDirection;
            inputDirection = Input.GetSwipe(out distance);

            if (Input.CurrentTouchState == Input.TouchState.Ended)
            {
                if (inputDirection == Input.Direction.Right && distance > 200)
                    ChangeScene(true);
                else if (inputDirection == Input.Direction.Left && distance > 200)
                    ChangeScene(false);
            }

            GameManager.Cam.Position = Vector2.Lerp(GameManager.Cam.Position, camTargetPos, (float)time.ElapsedGameTime.TotalSeconds * 12);
        }

        /// <summary>
        /// Changes Gui element at the buttom which indicates wether the player is in garage or market
        /// </summary>
        /// <param name="toMarket">Is it a transition to market?</param>
        public void ChangeScene(bool toMarket)
        {
            if (toMarket)
            {
                camTargetPos = new Vector2(-GameManager.GameSize.Width, 0);

                bottomBarGarage.Texture = Asset.Image["gui_bottombar_boxempty"];
                bottomBarMarket.Texture = Asset.Image["gui_bottombar_boxfilled"];
            }
            else
            {
                camTargetPos = Vector2.Zero;

                bottomBarGarage.Texture = Asset.Image["gui_bottombar_boxfilled"];
                bottomBarMarket.Texture = Asset.Image["gui_bottombar_boxempty"];
            }
        }

        /// <summary>
        /// Sets bottomBarCash.Text equal to the amount of money the <see cref="Player"/> instance has.
        /// </summary>
        /// <param name="time">Current game time.</param>
        /// <param name="sb">Current sprite batch.</param>
        public override void Draw(GameTime time, SpriteBatch sb)
        {
            if (bottomBarCash != null)
                bottomBarCash.Text = Player.Instance.Money.ToString();
        }
    }   
}
