﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using Microsoft.Xna.Framework.Input;

namespace OldMopeds
{
    public delegate void btnFunction();
    public delegate void btnFunctionWithInt(int intParam);
    public delegate void btnFunctionWithString(string stringParam);

    /// <summary>
    /// Interface subclass for clickable buttons.
    /// </summary>
    public class InterfaceButton : InterfaceElement
    {
        private btnFunction buttonFunctionNoParam;
        private btnFunctionWithInt buttonFunctionParamInt;
        private btnFunctionWithString buttonFunctionParamString;

        int intParam;
        string stringParam;

        private Texture2D texture;

        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InterfaceButton"/> class.
        /// </summary>
        /// <param name="localPosition">The InterfaceElement's position relative to its parent.</param>
        /// <param name="size">The <see cref="Size"/> of the element.</param>
        /// <param name="color">The <see cref="Color"/> to render the element with.</param>
        /// <param name="depth">The depth value to render with in the <see cref="SpriteBatch"/>.</param>
        /// <param name="drawInGUI">Whether to calculate position in screen space rather than world space.</param>
        /// <param name="parentElement">Potential parent <see cref="InterfaceElement"/>.</param>
        public InterfaceButton(Vector2 localPosition, Size size, Color color, float depth, bool drawInGUI = false, InterfaceElement parentElement = null)
            : base(localPosition, size, color, depth, drawInGUI, parentElement)
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="InterfaceButton"/> class.
        /// </summary>
        /// <param name="localPosition">The InterfaceElement's position relative to its parent.</param>
        /// <param name="size">The <see cref="Size"/> of the element.</param>
        /// <param name="textureName">String name of the texture to render.</param>
        /// <param name="color">The <see cref="Color"/> to render the element with.</param>
        /// <param name="depth">The depth value to render with in the <see cref="SpriteBatch"/>.</param>
        /// <param name="drawInGUI">Whether to calculate position in screen space rather than world space.</param>
        /// <param name="parentElement">Potential parent <see cref="InterfaceElement"/>.</param>
        public InterfaceButton(Vector2 localPosition, Size size, string textureName, Color color, float depth, bool drawInGUI = false, InterfaceElement parentElement = null)
            : base(localPosition, size, color, depth, drawInGUI, parentElement)
        {
            this.texture = Asset.Image[textureName];
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="InterfaceButton"/> class.
        /// </summary>
        /// <param name="localPosition">The InterfaceElement's position relative to its parent.</param>
        /// <param name="size">The <see cref="Size"/> of the element.</param>
        /// <param name="texture"><see cref="Texture2D"/> to render.</param>
        /// <param name="color">The <see cref="Color"/> to render the element with.</param>
        /// <param name="depth">The depth value to render with in the <see cref="SpriteBatch"/>.</param>
        /// <param name="drawInGUI">Whether to calculate position in screen space rather than world space.</param>
        /// <param name="parentElement">Potential parent <see cref="InterfaceElement"/>.</param>
        public InterfaceButton(Vector2 localPosition, Size size, Texture2D texture, Color color, float depth, bool drawInGUI = false, InterfaceElement parentElement = null)
            : base(localPosition, size, color, depth, drawInGUI, parentElement)
        {
            this.texture = texture;
        }

        /// <summary>
        /// Checks whether user clicks inside the element's <see cref="WorldRectangle"/>.
        /// </summary>
        /// <param name="time">Current game time.</param>
        public override void Update(GameTime time)
        {
            if (Input.CurrentTouchState == Input.TouchState.Click)
            {
                Vector2 mousePos = new Vector2(Input.MousePosition.X, Input.MousePosition.Y);

                if (!this.DrawInGUI)
                    mousePos = GameManager.Cam.ToVirtual(Input.MousePosition);

                if (WorldRectangle.Contains(mousePos))
	            {
                    Click();	 
	            }     
            }

            base.Update(time);
        }

        /// <summary>
        /// Adds a method to the button
        /// </summary>
        /// <param name="a">The method to add.</param>
        public void AddFunction(btnFunction buttonFunctionNoParam)
        {
            this.buttonFunctionNoParam = buttonFunctionNoParam;
        }
        
        /// <summary>
        /// Adds a function to the button, passed with an integer.
        /// </summary>
        /// <param name="buttonFunctionParamInt">The method to add.</param>
        /// <param name="intParam">The integer to pass.</param>
        public void AddFunction(btnFunctionWithInt buttonFunctionParamInt, int intParam)
        {
            this.buttonFunctionParamInt = buttonFunctionParamInt;
            this.intParam = intParam;
        }
        /// <summary>
        /// Adds a function to the button, passed with a string.
        /// </summary>
        /// <param name="buttonFunctionParamString">The method to add.</param>
        /// <param name="stringParam">The string to pass.</param>
        public void AddFunction(btnFunctionWithString buttonFunctionParamString, string stringParam)
        {
            this.buttonFunctionParamString = buttonFunctionParamString;
            this.stringParam = stringParam;
        }

        /// <summary>
        /// Executes the button function.
        /// </summary>
        private void Click()
        {
            if (buttonFunctionNoParam != null)
                buttonFunctionNoParam();

            if (buttonFunctionParamInt != null)
                buttonFunctionParamInt(intParam);

            if (buttonFunctionParamString != null)
                buttonFunctionParamString(stringParam);
        }

        // Der sker en weird fejl med Scrollers hvis ikke dette står:
        /// <summary>
        /// Draws the button.
        /// </summary>
        /// <param name="time">Current game time.</param>
        /// <param name="sb">SpriteBatch to draw with.</param>
        public override void Draw(GameTime time, SpriteBatch sb)
        {
            if (texture != null)
                sb.Draw(this.texture, this.WorldRectangle, null, this.color, 0, Vector2.Zero, SpriteEffects.None, this.depth);
            else
                sb.Draw(this.texPixel, this.WorldRectangle, null, this.color, 0, Vector2.Zero, SpriteEffects.None, this.depth);
        }
    }
}