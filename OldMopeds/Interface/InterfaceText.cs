﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace OldMopeds
{
    /// <summary>
    /// Interface subclass for drawing anchored text.
    /// </summary>
    public class InterfaceText : InterfaceElement
    {
        public enum Anchors {
            TopLeft, TopCenter, TopRight,
            MiddleLeft, MiddleCenter, MiddleRight,
            BottomLeft, BottomCenter, BottomRight
        };

        private string text;
        private SpriteFont spriteFont;
        private Anchors anchor = Anchors.TopLeft;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public SpriteFont SpriteFont
        {
            get { return spriteFont; }
            set { spriteFont = value; }
        }

        public Anchors Anchor
        {
            get { return anchor; }
            set { anchor = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InterfaceText"/> class.
        /// </summary>
        /// <param name="text">The string to write to the screen.</param>
        /// <param name="spriteFont">The <see cref="SpriteFont"/> to draw with.</param>
        /// <param name="alignment">The anchor position of the text element.</param>
        /// <param name="localPosition">The InterfaceElement's position relative to its parent.</param>
        /// <param name="color">The <see cref="Color"/> to render the element with.</param>
        /// <param name="depth">The depth value to render with in the <see cref="SpriteBatch"/>.</param>
        /// <param name="drawInGUI">Whether to calculate position in screen space rather than world space.</param>
        /// <param name="parentElement">Potential parent <see cref="InterfaceElement"/>.</param>
        public InterfaceText(string text, SpriteFont spriteFont, Anchors alignment, Vector2 localPosition, Color color, float depth, bool drawInGUI = false, InterfaceElement parentElement = null)
            : base(localPosition, color, depth, drawInGUI, parentElement)
        {
            this.text = text;
            this.spriteFont = spriteFont;
            this.anchor = alignment;
        }

        /// <summary>
        /// Draws the text element in relation to anchor point.
        /// </summary>
        /// <param name="time">Current game time.</param>
        /// <param name="sb">SpriteBatch to draw with.</param>
        public override void Draw(GameTime time, SpriteBatch sb)
        {
            Vector2 pos = this.WorldPosition;
            Vector2 stringMeasure = this.spriteFont.MeasureString(this.text);

            switch (this.anchor)
            {
                case Anchors.TopLeft:
                    break;
                case Anchors.TopCenter:
                    pos.X -= stringMeasure.X * 0.5f;
                    break;
                case Anchors.TopRight:
                    pos.X -= stringMeasure.X;
                    break;
                case Anchors.MiddleLeft:
                    pos.Y -= stringMeasure.Y * 0.5f;
                    break;
                case Anchors.MiddleCenter:
                    pos.X -= stringMeasure.X * 0.5f;
                    pos.Y -= stringMeasure.Y * 0.5f;
                    break;
                case Anchors.MiddleRight:
                    pos.X -= stringMeasure.X;
                    pos.Y -= stringMeasure.Y * 0.5f;
                    break;
                case Anchors.BottomLeft:
                    pos.Y -= stringMeasure.Y;
                    break;
                case Anchors.BottomCenter:
                    pos.X -= stringMeasure.X * 0.5f;
                    pos.Y -= stringMeasure.Y;
                    break;
                case Anchors.BottomRight:
                    pos.X -= stringMeasure.X;
                    pos.Y -= stringMeasure.Y;
                    break;
            }

            sb.DrawString(this.spriteFont, this.text, pos, this.color, 0, Vector2.Zero, 1, SpriteEffects.None, this.depth);
        }
    }
}
