﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace OldMopeds
{
    /// <summary>
    /// Interface subclass for pictures.
    /// </summary>
    public class InterfacePicture : InterfaceElement
    {
        private Texture2D texture;

        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InterfacePicture"/> class.
        /// </summary>
        /// <param name="pictureName">String name of the texture.</param>
        /// <param name="localPosition">The InterfaceElement's position relative to its parent.</param>
        /// <param name="size">The <see cref="Size"/> of the element.</param>
        /// <param name="color">The <see cref="Color"/> to render the element with.</param>
        /// <param name="depth">The depth value to render with in the <see cref="SpriteBatch"/>.</param>
        /// <param name="drawInGUI">Whether to calculate position in screen space rather than world space.</param>
        /// <param name="parentElement">Potential parent <see cref="InterfaceElement"/>.</param>
        public InterfacePicture(string pictureName, Vector2 localPosition, Size size, Color color, float depth, bool drawInGUI = false, InterfaceElement parentElement = null)
            : base(localPosition, size, color, depth, drawInGUI, parentElement)
        {
            this.texture = Asset.Image[pictureName];
            this.size = size;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="InterfacePicture"/> class.
        /// </summary>
        /// <param name="picture">Texture2D to render.</param>
        /// <param name="localPosition">The InterfaceElement's position relative to its parent.</param>
        /// <param name="size">The <see cref="Size"/> of the element.</param>
        /// <param name="color">The <see cref="Color"/> to render the element with.</param>
        /// <param name="depth">The depth value to render with in the <see cref="SpriteBatch"/>.</param>
        /// <param name="drawInGUI">Whether to calculate position in screen space rather than world space.</param>
        /// <param name="parentElement">Potential parent <see cref="InterfaceElement"/>.</param>
        public InterfacePicture(Texture2D picture, Vector2 localPosition, Size size, Color color, float depth, bool drawInGUI = false, InterfaceElement parentElement = null)
            : base(localPosition, size, color, depth, drawInGUI, parentElement)
        {
            this.texture = picture;
            this.size = size;
        }

        /// <summary>
        /// Draws the picture.
        /// </summary>
        /// <param name="time">Current game time.</param>
        /// <param name="sb">SpriteBatch to draw with.</param>
        public override void Draw(GameTime time, SpriteBatch sb)
        {
            sb.Draw(this.texture, this.WorldRectangle, null, this.color, 0, Vector2.Zero, SpriteEffects.None, this.depth);
        }
    }
}
