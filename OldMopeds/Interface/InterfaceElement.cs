﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace OldMopeds
{
    /// <summary>
    /// Interface superclass for drawing to screen. Can have multiple child elements or a single parent element.
    /// </summary>
    public class InterfaceElement : GameObject
    {
        private InterfaceElement parentElement;
        public List<InterfaceElement> childElements = new List<InterfaceElement>();

        protected Vector2 localPosition;
        protected Size size;
        protected Color color;
        protected float depth;
        protected Texture2D texPixel;

        public Vector2 LocalPosition
        {
            get { return localPosition; }
            set { localPosition = value; }
        }
        /// <summary>
        /// Returns the element's global position in world space calculated through the parent hierarchy.
        /// </summary>
        public Vector2 WorldPosition
        {
            get
            {
                if (parentElement != null)
                    return parentElement.WorldPosition + localPosition;
                else
                    return localPosition;
            }
        }
        public Size Size
        {
            get { return size; }
            set { size = value; }
        }
        public Rectangle WorldRectangle
        {
            get { 
                return new Rectangle(
                    (int)WorldPosition.X, (int)WorldPosition.Y, 
                         Size.Width,           Size.Height); 
            }
        }
        public Color Color
        {
            get { return color; }
            set { color = value; }
        }
        public float Depth
        {
            get { return depth; }
            set { depth = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InterfaceElement"/> class.
        /// </summary>
        /// <param name="localPosition">The InterfaceElement's position relative to its parent.</param>
        /// <param name="color">The <see cref="Color"/> to render the element with.</param>
        /// <param name="depth">The depth value to render with in the <see cref="SpriteBatch"/>.</param>
        /// <param name="drawInGUI">Whether to calculate position in screen space rather than world space.</param>
        /// <param name="parentElement">Potential parent <see cref="InterfaceElement"/>.</param>
        public InterfaceElement(Vector2 localPosition, Color color, float depth, bool drawInGUI = false, InterfaceElement parentElement = null) : base()
        {
            this.localPosition = localPosition;
            this.color = color;
            this.depth = depth;
            this.texPixel = Asset.Image["pixel"];
            this.DrawInGUI = drawInGUI;

            this.parentElement = parentElement;
            if (this.parentElement != null)
                this.parentElement.childElements.Add(this);
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="InterfaceElement"/> class.
        /// </summary>
        /// <param name="localPosition">The InterfaceElement's position relative to its parent.</param>
        /// <param name="size">The <see cref="Size"/> of the element.</param>
        /// <param name="color">The <see cref="Color"/> to render the element with.</param>
        /// <param name="depth">The depth value to render with in the <see cref="SpriteBatch"/>.</param>
        /// <param name="drawInGUI">Whether to calculate position in screen space rather than world space.</param>
        /// <param name="parentElement">Potential parent <see cref="InterfaceElement"/>.</param>
        public InterfaceElement(Vector2 localPosition, Size size, Color color, float depth, bool drawInGUI = false, InterfaceElement parentElement = null) : base()
        {
            this.localPosition = localPosition;
            this.size = size;
            this.color = color;
            this.depth = depth;
            this.texPixel = Asset.Image["pixel"];
            this.DrawInGUI = drawInGUI;

            this.parentElement = parentElement;
            if (this.parentElement != null)
                this.parentElement.childElements.Add(this);
        }

        /// <summary>
        /// Draws the element.
        /// </summary>
        /// <param name="time">Current game time.</param>
        /// <param name="sb">Current <see cref="SpriteBatch"/>.</param>
        public override void Draw(GameTime time, SpriteBatch sb)
        {
            sb.Draw(this.texPixel, this.WorldRectangle, null, this.color, 0, Vector2.Zero, SpriteEffects.None, this.depth);
        }

        /// <summary>
        /// Unused.
        /// </summary>
        /// <param name="time">Current game time.</param>
        public override void Update(GameTime time)
        {
        }

        /// <summary>
        /// Disposes all child elements before disposing self.
        /// </summary>
        /// <param name="forced">Did it not happen automatically?</param>
        protected override void Dispose(bool forced)
        {
            foreach (var child in childElements)
            {
                child.Dispose();
            }

            base.Dispose(forced);
        }
    }
}
