﻿using Microsoft.Xna.Framework;

namespace OldMopeds
{
    /// <summary>
    /// Interface subclass for scrollable element.
    /// </summary>
    class InterfaceScroll : InterfaceElement
    {
        private float? mouseOffsetY;
        private float lastPosY;
        private float deltaY;
        private float releaseVelY;

        /// <summary>
        /// Initializes a new instance of the <see cref="InterfaceScroll"/> class.
        /// </summary>
        /// <param name="localPosition">The InterfaceElement's position relative to its parent.</param>
        /// <param name="size">The <see cref="Size"/> of the element.</param>
        /// <param name="color">The <see cref="Color"/> to render the element with.</param>
        /// <param name="depth">The depth value to render with in the <see cref="SpriteBatch"/>.</param>
        /// <param name="drawInGUI">Whether to calculate position in screen space rather than world space.</param>
        /// <param name="parentElement">Potential parent <see cref="InterfaceElement"/>.</param>
        public InterfaceScroll(Vector2 localPosition, Size size, Color color, float depth, bool drawInGUI = false, InterfaceElement parentElement = null)
            : base(localPosition, size, color, depth, drawInGUI, parentElement)
        {
        }

        /// <summary>
        /// If user clicks and holds within <see cref="WorldRectangle"/> the scroll element follows the cursor, clamped within GameSize.
        /// </summary>
        /// <param name="time">Current game time.</param>
        public override void Update(GameTime time)
        {
            // Disable scroll if size is smaller than GameSize.Height
            if (this.size.Height < GameManager.GameSize.Height)
                return;

            Vector2 mousePos = GameManager.Cam.ToVirtual(Input.MousePosition);

            // Check for touch begin inside rectangle - set mouse offset
            if (WorldRectangle.Contains(mousePos))
            {
                if (mouseOffsetY == null && Input.CurrentTouchState == Input.TouchState.Begin)
                    mouseOffsetY = mousePos.Y - localPosition.Y;
            }

            // Check for touch ended - clear mouse offset, set releaseVel / lerp releaseVel
            if (Input.CurrentTouchState == Input.TouchState.Ended)
            {
                if (mouseOffsetY != null)
                {
                    mouseOffsetY = null;
                    releaseVelY = deltaY;
                }
                else
                {
                    releaseVelY = MathHelper.Lerp(releaseVelY, 0, (float)time.ElapsedGameTime.TotalSeconds * 10);
                }
            }

            // Touch drag Scroll
            if (mouseOffsetY != null)
                localPosition.Y = mousePos.Y - (float)mouseOffsetY;

            // Clamp within GameSize
            deltaY = localPosition.Y - lastPosY;
            localPosition.Y += releaseVelY;
            localPosition.Y =
                MathHelper.Clamp(localPosition.Y,
                    -GameManager.Cam.ToVirtual(size.ToPoint()).Y,
                        GameManager.Cam.ToVirtual(Point.Zero).Y);

            lastPosY = localPosition.Y;

            base.Update(time);
        }
    }
}
