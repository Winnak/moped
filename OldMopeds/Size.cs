﻿using System;
using Microsoft.Xna.Framework;

namespace OldMopeds
{
    /// <summary>
    /// Container with width and height information.
    /// </summary>
    public struct Size : IEquatable<Size>
    {
        private int width, height;

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        public int Width
        {
            get { return this.width; }
            set { this.width = value; }
        }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        public int Height
        {
            get { return this.height; }
            set { this.height = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Size"/> struct.
        /// </summary>
        /// <param name="size">The width and height.</param>
        public Size(int size)
        {
            this.width = size;
            this.height = size;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Size" /> struct.
        /// </summary>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        public Size(int width, int height)
        {
            this.width = width;
            this.height = height;
        }

        #region Operators
        public static Size operator +(Size s1, Size s2)
        {
            return new Size(s1.width + s2.width, s1.height + s2.height);
        }

        public static Size operator -(Size s1, Size s2)
        {
            return new Size(s1.width - s2.width, s1.height - s2.height);
        }

        public static Size operator *(Size s1, int a)
        {
            return new Size(s1.width * a, s1.height * a);
        }

        public static Size operator *(int a, Size v1)
        {
            return v1 * a;
        }
        #endregion

        /// <summary>
        /// Gets the <see cref="Size"/> as a string.
        /// </summary>
        /// <returns><see cref="width"/> and <see cref="height"/> as a string.</returns>
        public override string ToString()
        {
            return string.Format("{0}, {1}", this.width, this.height);
        }

        /// <summary>
        /// Gets the center of the size.
        /// </summary>
        /// <returns>The center as a vector2.</returns>
        public Vector2 Center
        {
            get { return new Vector2(this.width * 0.5f, this.height * 0.5f); }
        }

        /// <summary>
        /// Checks if the sizes are equal.
        /// </summary>
        /// <param name="other">Other size</param>
        /// <returns></returns>
        public bool Equals(Size other) 
        {
            return this.width == other.width && this.height == other.height;
        }

        /// <summary>
        /// Checks if objects are eqal
        /// </summary>
        /// <param name="obj">Other object</param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj is Size)
            {
                return Equals((Size)this);
            }

            return false;
        }

        /// <summary>
        /// Gets the hash code of the objects members
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            return this.width.GetHashCode() + this.height.GetHashCode();
        }

        /// <summary>
        /// Converts <see cref="Size"/> to a <see cref="Vector2"/> object.
        /// </summary>
        /// <returns>Vector2 object.</returns>
        public Vector2 ToVector2()
        {
            return new Vector2(this.width, this.height);
        }

        /// <summary>
        /// Converts <see cref="Size"/> to a <see cref="Size"/> object.
        /// </summary>
        /// <returns>Point object.</returns>
        public Point ToPoint()
        {
            return new Point(this.width, this.height);
        }
    }
}
