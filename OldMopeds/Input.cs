﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;


namespace OldMopeds
{
    /// <summary>
    /// Manages all touch input.
    /// </summary>
    public static class Input
    {
        public enum Direction {None = 0, Left = 1, Right = 2, Up = 3, Down = 4};

        public enum TouchState { Click, Moved, Begin, Ended };
        private static MouseState mouseState;

        private static TouchState currentState = TouchState.Ended;

        private static Point beginMousePos = Point.Zero;
        private static Point endMousePos   = Point.Zero;

        public static TouchState CurrentTouchState
        {
            get { return currentState; }
        }
#if !LINUX
        public static Point MousePosition
        {
            get { return mouseState.Position; } // "New feature" i monogame 3.2
        }
#else
		public static Point MousePosition {
			get { return new Point(mouseState.X, mouseState.Y); }
		}
#endif
        /// <summary>
        /// Checks for input.
        /// </summary>
        /// <param name="time">Current game time.</param>
        public static void Update(GameTime time)
        {
            mouseState = Mouse.GetState();
            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                if (currentState == TouchState.Ended)
                {
                    beginMousePos = MousePosition;
                    endMousePos   = beginMousePos;
                    currentState  = TouchState.Begin;
                }

                if (currentState == TouchState.Begin)
                {
                    var begin = beginMousePos.X * beginMousePos.X 
                              + beginMousePos.Y * beginMousePos.Y;

                    var current = MousePosition.X * MousePosition.X
                                + MousePosition.Y * MousePosition.Y;

                    if (Math.Abs(begin - current) > 35000) // mål i Sagans, svare til c. 58px
                    {
                        endMousePos = MousePosition;
                        currentState = TouchState.Moved;
                    }
                }
            }
            else
            {
                switch (currentState)
                {
                    case TouchState.Click:
                    case TouchState.Moved:
                        endMousePos = MousePosition;
                        currentState = TouchState.Ended;
                        break;
                    case TouchState.Begin:
                        beginMousePos = endMousePos;
                        currentState = TouchState.Click;
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Gets a swipe input, between holding and releasing a touchstate.
        /// </summary>
        /// <returns>The direction of the swipe.</returns>
        public static Direction GetSwipe()
        {
#if LINUX
			var result = new Point(endMousePos.X - beginMousePos.X, 
			                       endMousePos.Y - beginMousePos.Y);
#else
            var result = endMousePos - beginMousePos;
#endif
            if (result == Point.Zero)
            {
                return Direction.None;
            }

            if (Math.Abs(result.X) < Math.Abs(result.Y))
            {
                if (result.Y < 0)
                    return Direction.Up;
                else
                    return Direction.Down;
            }
            else
            {
                if (result.X < 0)
                    return Direction.Left;
                else
                    return Direction.Right;
            }
        }

        /// <summary>
        /// Gets a swipe input, between holding and releasing a touchstate.
        /// </summary>
        /// <param name="deltaLength">The length of the swipe.</param>
        /// <returns>The direction of the swipe</returns>
        public static Direction GetSwipe(out double deltaLenght)
        {
#if LINUX
			var result = new Point(endMousePos.X - beginMousePos.X, 
			                       endMousePos.Y - beginMousePos.Y);
#else
            var result = endMousePos - beginMousePos;
#endif
            deltaLenght = Math.Sqrt(result.X * result.X + result.Y * result.Y);

            if (result == Point.Zero)
            {
                return Direction.None;
            }

            if (Math.Abs(result.X) < Math.Abs(result.Y)) 
            {
                if (result.Y < 0)
                    return Direction.Up;
                else
                    return Direction.Down;
            }
            else
            {
                if (result.X < 0)
                    return Direction.Left;
                else
                    return Direction.Right;
            }
        }

        /// <summary>
        /// Gets a swipe input, between holding and releasing a touchstate.
        /// </summary>
        /// <param name="deltaPos">The delta position of the swipe.</param>
        /// <returns>The direction of the swipe.</returns>
        public static Direction GetSwipe(out Point deltaPos)
        {
#if LINUX
			var result = new Point(endMousePos.X - beginMousePos.X, 
			                       endMousePos.Y - beginMousePos.Y);
#else
            var result = endMousePos - beginMousePos;
#endif
            deltaPos = result;

            if (result == Point.Zero)
            {
                return Direction.None;
            }

            if (Math.Abs(result.X) < Math.Abs(result.Y))
            {
                if (result.Y < 0)
                    return Direction.Up;
                else
                    return Direction.Down;
            }
            else
            {
                if (result.X < 0)
                    return Direction.Left;
                else
                    return Direction.Right;
            }
        }
    }
}
