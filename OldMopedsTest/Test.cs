﻿using NUnit.Framework;
using System;
using OldMopeds;

namespace OldMopedsTest
{
    /* <summary>
     * NUnit test (because Erik had troubles with MS unit tests.
     * To get it to work you must either get the nessercersary files from nuget
     * or use xamarian/mono develop to compile.
     */
	[TestFixture ()]
	public class Test
	{
		[Test ()]
		public void TestReadParameters()
		{
			//List intanziated in order to have an actual list to add the moped to. 
			//Otherwise the moped attempts to add itself to a nonexisting list,
			//thus creating a null exception
			GameManager.Objects = new System.Collections.Generic.List<GameObject>();

			Moped moped = new Moped("Old Moped", "speed=1", "friction=5", "weight=6", "acceleration=2", "aerodynamism=3");

			Assert.AreEqual(1, moped.BaseSpeed);
			Assert.AreEqual(1, moped.BaseSpeed);
			Assert.AreEqual(5, moped.BaseFriction);
			Assert.AreEqual(6, moped.BaseWeight);
			Assert.AreEqual(2, moped.BaseAcceleration);
			Assert.AreEqual(3, moped.BaseAerodynamism);
			Assert.AreEqual("Old Moped", moped.Name);
		}

		[Test ()]
		public void TestReadParameteresComp()
		{
			//List intanziated in order to have an actual list to add the moped to. 
			//Otherwise the moped attempts to add itself to a nonexisting list,
			//thus creating a null exception
			GameManager.Objects = new System.Collections.Generic.List<GameObject>();

			Component comp = new Component("Old Moped", Component.ComponentType.Engine, "speed=1", "friction=5", "weight=6", "acceleration=2", "aerodynamism=3");

			Assert.AreEqual(1, comp.Speed);
			Assert.AreEqual(5, comp.Friction);
			Assert.AreEqual(6, comp.Weight);
			Assert.AreEqual(2, comp.Acceleration);
			Assert.AreEqual(3, comp.Aerodynamism);


		}

		[Test ()]
		public void TestUpdateStats()
		{
			//List intanziated in order to have an actual list to add the moped to. 
			//Otherwise the moped attempts to add itself to a nonexisting list,
			//thus creating a null exception
			GameManager.Objects = new System.Collections.Generic.List<GameObject>();

			Moped moped = new Moped("Old Moped", "speed=1", "friction=5", "weight=6", "acceleration=2", "aerodynamism=3");

			Assert.AreEqual(6, moped.MaxSpeed);
			Assert.AreEqual(10, moped.Friction);
			Assert.AreEqual(11, moped.Weight);
			Assert.AreEqual(7, moped.Acceleration);
			Assert.AreEqual(8, moped.Aerodynamism);
		}

		[Test ()]
		public void TestMopedWithComponent() // Denne test fejler, mopeds stats bliver ikke erstattet
		{
			//List intanziated in order to have an actual list to add the moped to. 
			//Otherwise the moped attempts to add itself to a nonexisting list,
			//thus creating a null exception
		    GameManager.Objects = new System.Collections.Generic.List<GameObject>();

		    Moped moped = new Moped("Old Moped");
			moped.ChangeComponent(new Component("Wheel", Component.ComponentType.Wheel, "friction=3", "acceleration=2"));

			Assert.AreEqual(6, moped.Acceleration);
			Assert.AreEqual(7, moped.Friction);
		}
	}
}

