﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OldMopeds;

namespace TestOldMopeds
{
    [TestClass]
    public class MopedTest
    {
        [TestMethod]
        public void TestReadParameters()
        {
            //List intanziated in order to have an actual list to add the moped to. 
            //Otherwise the moped attempts to add itself to a nonexisting list,
            //thus creating a null exception
            GameManager.Objects = new System.Collections.Generic.List<GameObject>();

            Moped moped = new Moped("Old Moped", "speed=1", "friction=5", "weight=6", "acceleration=2", "aerodynamism=3");

            Assert.AreEqual(1, moped.BaseSpeed);
            Assert.AreEqual(1, moped.BaseSpeed);
            Assert.AreEqual(5, moped.BaseFriction);
            Assert.AreEqual(6, moped.BaseWeight);
            Assert.AreEqual(2, moped.BaseAcceleration);
            Assert.AreEqual(3, moped.BaseAerodynamism);
            Assert.AreEqual("Old Moped", moped.Name);
        }

        [TestMethod]
        public void TestReadParameteresComp()
        {
            //List intanziated in order to have an actual list to add the moped to. 
            //Otherwise the moped attempts to add itself to a nonexisting list,
            //thus creating a null exception
            GameManager.Objects = new System.Collections.Generic.List<GameObject>();

            Component comp = new Component("Old Moped", Component.ComponentType.Engine, "speed=1", "friction=5", "weight=6", "acceleration=2", "aerodynamism=3");

            Assert.AreEqual(1, comp.Speed);
            Assert.AreEqual(5, comp.Friction);
            Assert.AreEqual(6, comp.Weight);
            Assert.AreEqual(2, comp.Acceleration);
            Assert.AreEqual(3, comp.Aerodynamism);


        }

        [TestMethod]
        public void TestUpdateStats()
        {
            //List intanziated in order to have an actual list to add the moped to. 
            //Otherwise the moped attempts to add itself to a nonexisting list,
            //thus creating a null exception
            GameManager.Objects = new System.Collections.Generic.List<GameObject>();

            Moped moped = new Moped("Old Moped", "speed=1", "friction=5", "weight=6", "acceleration=2", "aerodynamism=3");

            Assert.AreEqual(6, moped.MaxSpeed);
            Assert.AreEqual(10, moped.Friction);
            Assert.AreEqual(11, moped.Weight);
            Assert.AreEqual(7, moped.Acceleration);
            Assert.AreEqual(8, moped.Aerodynamism);
        }
    }
}
